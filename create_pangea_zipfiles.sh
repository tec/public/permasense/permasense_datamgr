#!/bin/bash
# Script to prepare zip files for pangea upload

DESTINATION=/ifi-NAS/nes/research/permasense_datamgr/data
#DESTINATION=/ifi-NAS/nes/research/permasense_datamgr/mh_data

ORIGIN=/ifi-NAS/nes/research/gps/rtklib_processing
ZIPFLAGS='-r -q'
ENDYEAR=$(date +%Y -d "1 year ago")

cd ${DESTINATION}

echo 'gnss_data_raw'
zip ${ZIPFLAGS} ${DESTINATION}/gnss_data_raw.zip gnss_data_raw/*.csv
# zip -s 20g ${ZIPFLAGS} ${DESTINATION}/gnss_data_raw ${DESTINATION}/gnss_data_raw/*

echo 'gnss_data_raw_observables'
zip -s 10g ${ZIPFLAGS} ${DESTINATION}/gnss_data_raw_observables.zip gnss_data_raw/* --exclude *.csv



echo 'gnss_derived_data_products'
zip ${ZIPFLAGS} ${DESTINATION}/gnss_derived_data_products.zip gnss_derived_data_products/*

# for (( i=2010; i<=$ENDYEAR; i++ )) 
# do 
#     echo 'timelapse_images' $i
#     zip ${ZIPFLAGS} ${DESTINATION}/timelapse_images_${i}.zip ${DESTINATION}/timelapse_images/${i}-*
# done

echo 'timeseries_data_raw'
zip -s 15g ${ZIPFLAGS} ${DESTINATION}/timeseries_data_raw timeseries_data_raw/*
echo 'timeseries_derived data_products'
zip ${ZIPFLAGS} ${DESTINATION}/timeseries_derived_data_products.zip timeseries_derived_data_products/*
echo 'timeseries_sanity_plots'
zip ${ZIPFLAGS} ${DESTINATION}/timeseries_sanity_plots.zip timeseries_sanity_plots*/*

ls -al $DESTINATION
