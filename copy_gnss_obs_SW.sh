#!/bin/bash

DESTINATION=/ifi-NAS/nes/research/permasense_datamgr/data/gnss_data_raw

ORIGIN=/ifi-NAS/nes/research/gps/rtklib_processing
# ORIGIN=/scratch/jan.beutel/gps/rtklib_processing

STATIONS='RAND HOGR'
# STATIONS='HOGR'

for NODE in $STATIONS; do
    mkdir -p ${DESTINATION}/${NODE}
    #ls ${ORIGIN}/cogear/${NODE}/${NODE}*.*O
    ls ${ORIGIN}/cogear
    rsync -t ${ORIGIN}/cogear/${NODE}/${NODE}*.*O ${DESTINATION}/${NODE}
done

STATIONS='MH33 MH34 MH35 MH40 MH43'

for NODE in $STATIONS; do
    mkdir -p ${DESTINATION}/${NODE}
    #ls ${ORIGIN}/matterhorn/${NODE}/${NODE}*.*O
    ls ${ORIGIN}/matterhorn
    rsync -t ${ORIGIN}/matterhorn/${NODE}/${NODE}*.*O ${DESTINATION}/${NODE}
done

STATIONS='COR1 DIS1 DIS2 GRU1 JAE1 LAR1 LAR2 MUA1 RIT1 SCH1'

for NODE in $STATIONS; do
    mkdir -p ${DESTINATION}/${NODE}
    #ls ${ORIGIN}/permos/${NODE}/${NODE}*.*O
    ls ${ORIGIN}/permos
    rsync -t ${ORIGIN}/permos/${NODE}/${NODE}*.*O ${DESTINATION}/${NODE}
done

STATIONS='RD01 DI55 RA01 DI57 RA02 RA03 BH03 DI02 DI07 LS01 LS04 BH07 BH09 ST02 ST05 GU02 GU03 RG01 GG52 GG01 GG02 BH10 RL01 GU04 BH12 BH13 LS05 GG66 GG67 RA03 WYS1 LS11 LS12 DI03 DI04 LS06 SA01 SATT'

for NODE in $STATIONS; do
    mkdir -p ${DESTINATION}/${NODE}
    #ls ${ORIGIN}/permos/${NODE}/${NODE}*.*O
    ls ${ORIGIN}/dirruhorn
    rsync -t ${ORIGIN}/dirruhorn/${NODE}/${NODE}*.*O ${DESTINATION}/${NODE}
done

ls $DESTINATION
