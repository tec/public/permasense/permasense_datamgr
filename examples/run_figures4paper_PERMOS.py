"""
Script to create the figures for the paper "XXXXXXXXXXXXXX"

Copyright (c) 2021, Swiss Federal Institute of Technology (ETH Zurich)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@date: March 31, 2021
@author: Samuel Weber, Jan Beutel
"""
# Import Libraries
import sys, inspect
import socket
import os as os
import numpy as np
import pandas as pd
import datetime as dt
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.transforms as mtransforms

# remove matplotlib warning 
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

# Load functions in relative path
# pwd = os.path.dirname(os.path.abspath(os.getcwd()))
pwd = os.path.dirname(os.path.abspath(__file__))
print(pwd)

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir) 

# sys.path.append('/home/perma/permasense_vault/scratch/essd_zenodo_datamgr/')
from collections import OrderedDict
from permasense.GSNdata import get_GSNdata, save_data, load_data, filter_data, clean_data
from permasense.plotting import dataavailability_flags, nodata_rectangles, colorbar_hydrologicalyears

os.chdir(pwd)

# For proper plotting on server
if socket.gethostname() == 'tik43x':
    mpl.use('Agg')

ALL_FIG = False
fig_weatherstation = True
fig_temperature_displacement = True
fig_dataavailability = True

PATH_DATA = './../permos_data/'
PATH_FIGURES = './figures_permos/'
print(PATH_DATA)
# Create PATH_DATA if it does not exist yet
os.makedirs(PATH_FIGURES, exist_ok=True)    

year_list = list(range(2008, dt.datetime.today().year + 1))
color_list = mpl.cm.tab10(np.linspace(0, 1, len(year_list))) # OR itertools.cycle(["r", "b", "g"]) with import itertools
if len(year_list) <= 10: 
    color_list = mpl.cm.tab10(range(0, len(year_list))) # OR itertools.cycle(["r", "b", "g"]) with import itertools
elif len(year_list) > 10:
    color_list = mpl.cm.tab20(range(0, len(year_list))) # OR itertools.cycle(["r", "b", "g"]) with import itertools

years = mdates.YearLocator()   # every year
biyearly =mdates.YearLocator(2)
months = mdates.MonthLocator()  # every month
yearsFmt = mdates.DateFormatter('%Y')

xpos_ylabel_square = -0.14

plt.rcParams.update({'font.size': 12})

color1 = 'black' #color = 'deepskyblue'
color2 = 'C2'#'deepskyblue' #color = 'deepskyblue'

# custom_date_parser = lambda x: pd.to_datetime(x, format="%Y-%m-%d %H:%M:%S")

if fig_dataavailability | ALL_FIG:
    DEPO_VS = {
            'DIS1': {'gps_differential__batch__daily','gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            'DIS2': {'gps_differential__batch__daily','gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            'RIT1': {'gps_differential__batch__daily','gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            'GRU1': {'gps_differential__batch__daily','gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            'JAE1': {'gps_differential__batch__daily','gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            'SCH1': {'gps_differential__batch__daily','gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            'MUA1': {'gps_differential__batch__daily','gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            'LAR1': {'gps_differential__batch__daily','gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            'LAR2': {'gps_differential__batch__daily','gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            'COR1': {'gps_differential__batch__daily','gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            }
    
    #DEPO_VS = sorted(DEPO_VS1.items(), key=lambda x: x[0])

    no_subplots = 0
    for depo in DEPO_VS:
        for vsensor in list(DEPO_VS[depo]):
            no_subplots += 1

    fig, ax = plt.subplots(no_subplots,1, figsize=(25/2.54, 28/2.54), facecolor='w', edgecolor='k', sharex=True, sharey=True)
    plt.subplots_adjust(left=0.26, right=0.97, bottom=0.03, top=0.99, hspace=0) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
        
    xpos_ylabel = -0.36

    #ax[0].set_xlim([dt.datetime(2008,9,1), dt.datetime(dt.datetime.today().year,1,1)])
    ax[0].set_xlim([dt.datetime(2010,10,1), dt.datetime(dt.datetime.today().year+1,1,1)])
    ax[0].set_yticks([])
        
    i=0
    
    for depo in DEPO_VS:
        # print(depo)
        # print(list(DEPO_VS[depo]))

        # Loop through values for a given key of dictionary DEPO_VS
        for vsensor in list(DEPO_VS[depo]):
            # print(vsensor)
            df = pd.DataFrame()
            if df.empty:
                try:
                    df = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
                except:
                    pass
            if df.empty:
                try:
                    df = load_data(PATH_DATA + 'gnss_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
                except:
                    pass

            ax[i].set_facecolor('darkgreen')
            
            try:
                t1, t2 = dataavailability_flags(df['position []'], daily=True)
            except:
                t1 = [pd.DataFrame([1], index=[dt.datetime(1970,1,1,1)]).index[0]]
                t2 = [pd.DataFrame([1], index=[dt.datetime(2050,1,1,1)]).index[0]]
                pass
                
            nodata_rectangles(ax[i], t1, t2, facecolor='white', alpha=1)
            
            if vsensor.startswith('gps_differential'):
                ax[i].set_ylabel('{:s} {:s}__{:s}'.format(depo, vsensor.split('__')[0], vsensor.split('__')[1]), rotation=0, ha='left')
            else:
                ax[i].set_ylabel('{:s} {:s}'.format(depo, vsensor.split('__')[0]), rotation=0, ha='left')
            
            ax[i].yaxis.set_label_coords(xpos_ylabel, 0.1)
            ax[i].grid()
            i += 1
                
            
        print(i)
        fname = 'fig_derived_data_availability.png'
        plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,


    no_subplots = 0

    for depo in DEPO_VS:
        for vsensor in list(DEPO_VS[depo]):
            if not vsensor.startswith('gps_differential__batch'):
                no_subplots += 1


    fig, ax = plt.subplots(no_subplots,1, figsize=(25/2.54, 18/2.54), facecolor='w', edgecolor='k', sharex=True, sharey=True)
    plt.subplots_adjust(left=0.26, right=0.97, bottom=0.06, top=0.99, hspace=0) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2

    ax[0].set_xlim([dt.datetime(2010,10,1), dt.datetime(dt.datetime.today().year+1,1,1)])
    ax[0].set_yticks([])
        
    i=0
    
    for depo in DEPO_VS:
        # print(depo)
        # print(list(DEPO_VS[depo]))

        # Loop through values for a given key of dictionary DEPO_VS
        for vsensor in sorted(list(DEPO_VS[depo])):
            print(depo, vsensor)
            if not vsensor.startswith('gps_differential__batch'):
                df = pd.DataFrame()
                if df.empty:
                    try:
                        df = load_data(PATH_DATA + 'timeseries_data_raw/', depo, vsensor, year=year_list, file_type='csv')
                        # print(len(df))
                    except:
                        pass
                if df.empty:
                    try:
                        df = load_data(PATH_DATA + 'gnss_data_raw/', depo, vsensor, year=year_list, file_type='csv')
                        # print(len(df))
                    except:
                        pass

                ax[i].set_facecolor('darkgreen')
                
                #print(vsensor)
                if vsensor.startswith('gps_differential__rtklib'):
                    max_date = max(df.index)
                    min_date = min(df.index)

                    df_gnss = pd.DataFrame()
                    file_dates = []

                    while min_date <= max_date:
                        min_year = min_date.strftime('%y')
                        min_doy = min_date.strftime('%j')

                        gnss_filename = PATH_DATA + 'gnss_data_raw/' + depo + '/' + depo + min_doy + '0.' + min_year + 'O'
                        # print(gnss_filename)

                        if os.path.isfile(gnss_filename):
                            #print ("File exist")
                            file_dates.append(pd.to_datetime(min_date))
                        #else:
                            #print ("File does not exist")

                        #print(pd.to_datetime(min_date))

                        min_date = min_date + dt.timedelta(days=1)
                        #print(min_date)
                    
                    df_gnss['time'] = file_dates
                    df_gnss['position []'] = file_dates
                    df_gnss = df_gnss.set_index('time')
                    try:
                        t1, t2 = dataavailability_flags(df_gnss['position []'], daily=True)
                    except:
                        t1 = [pd.DataFrame([1], index=[dt.datetime(1970,1,1,1)]).index[0]]
                        t2 = [pd.DataFrame([1], index=[dt.datetime(2050,1,1,1)]).index[0]]
                        pass
                # elif vsensor.startswith('gps_inclinometer'):
                else:
                    try:
                        t1, t2 = dataavailability_flags(df['position []'], daily=True)
                    except:
                        t1 = [pd.DataFrame([1], index=[dt.datetime(1970,1,1,1)]).index[0]]
                        t2 = [pd.DataFrame([1], index=[dt.datetime(2050,1,1,1)]).index[0]]
                        pass
                # else:
                #     pass
                    
                try:
                        nodata_rectangles(ax[i], t1, t2, facecolor='white', alpha=1)
                except:
                    pass

                if vsensor.startswith('gps_differential'):
                    ax[i].set_ylabel('{:s} gps_raw'.format(depo), rotation=0, ha='left')
                else:
                    ax[i].set_ylabel('{:s} {:s}'.format(depo, vsensor.split('__')[0]), rotation=0, ha='left')
                
                ax[i].yaxis.set_label_coords(xpos_ylabel, 0.1)
                ax[i].grid()
                i += 1

        print(i)
        fname = 'fig_raw_data_availability.png'
        plt.savefig(PATH_FIGURES + fname, format='png', dpi=300)

