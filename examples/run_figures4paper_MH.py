"""
Script to create the figures for the paper 
"A decade of detailed observations (2008--2018) in steep 
bedrock permafrost at Matterhorn Hoernligrat (Zermatt, CH)"

Copyright (c) 2021, Swiss Federal Institute of Technology (ETH Zurich)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@date: March 31, 2021
@author: Samuel Weber, Jan Beutel
"""
# Import Libraries
import sys, inspect
import socket
import math
import os as os
import numpy as np
import pandas as pd
import datetime as dt
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.transforms as mtransforms
from pandas.core.frame import DataFrame

# remove matplotlib warning 
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
mpl.rc('figure', max_open_warning = 0)

# Load functions in relative path
# pwd = os.path.dirname(os.path.abspath(os.getcwd()))
pwd = os.path.dirname(os.path.abspath(__file__))
print(pwd)
# os.chdir('..')

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir) 

# sys.path.append('/home/perma/permasense_vault/scratch/essd_zenodo_datamgr/')
from collections import OrderedDict
from permasense.GSNdata import get_GSNdata, save_data, load_data, filter_data, clean_data
from permasense.plotting import dataavailability_flags, nodata_rectangles, colorbar_hydrologicalyears, testbinaryfiles

os.chdir(pwd)

# For proper plotting on server
if socket.gethostname() == 'tik43x':
    mpl.use('Agg')

ALL_FIG = False
fig_weatherstation = True
fig_rocktemperature_cryosphere = True
fig_rocktemperature = True
fig_displacement = True
fig_displacements_cryosphere = True
fig_displacement_gps = True
fig_resistivity = True
fig_gps = True
fig_DRC = True
fig_dataavailability = True

fig_trial = False

PATH_DATA = '../mh_data/'
PATH_FIGURES = './figures_mh/'
# print(PATH_DATA)
# Create PATH_DATA if it does not exist yet
os.makedirs(PATH_FIGURES, exist_ok=True)    

year_list = list(range(2008, dt.datetime.today().year + 1))
color_list = mpl.cm.tab10(np.linspace(0, 1, len(year_list))) # OR itertools.cycle(["r", "b", "g"]) with import itertools
if len(year_list) <= 10: 
    color_list = mpl.cm.tab10(range(0, len(year_list))) # OR itertools.cycle(["r", "b", "g"]) with import itertools
elif len(year_list) > 10:
    color_list = mpl.cm.tab20(range(0, len(year_list))) # OR itertools.cycle(["r", "b", "g"]) with import itertools

years = mdates.YearLocator()   # every year
biyearly =mdates.YearLocator(2)
months = mdates.MonthLocator()  # every month
yearsFmt = mdates.DateFormatter('%Y')

xpos_ylabel_square = -0.14

plt.rcParams.update({'font.size': 12})

color1 = 'black' #color = 'deepskyblue'
color2 = 'C2'#'deepskyblue' 
color3 = 'deepskyblue'

#downsampling_freq = '1Y'
#downsampling_freq = '1W' #weekly
downsampling_freq = '1440Min' #daily
# downsampling_freq = '10080Min'


#%% HOGR, MH33-MH40: displacement GPS/inclinometer vs. time
if fig_displacement_gps | ALL_FIG:
    fig, ax = plt.subplots(3,1, figsize=(25/2.54, 25/2.54), facecolor='w', edgecolor='k', sharex=True)
    plt.subplots_adjust(left=0.09, right=0.995, bottom=0.1, top=0.99, hspace=0.05) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    
    #downsampling_freq = '1W' #weekly
    # downsampling_freq = '1440Min' #daily
    downsampling_freq = '10080Min' #weekly

    depo = 'HOGR'
    vsensor = 'gps_differential__rtklib__daily'
    #vsensor = 'gps_differential__batch__daily'
    df = load_data(PATH_DATA + 'gnss_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
    df  = df.groupby(pd.Grouper(freq=downsampling_freq)).aggregate(np.mean)        
    ax[0].plot((df['e [m]']-df['e [m]'][-1])*1000, color=color1, label='HOGR')
    ax[1].plot((df['n [m]']-df['n [m]'][-1])*1000, color=color1, label='HOGR')
    ax[2].plot((df['h [m]']-df['h [m]'][-1])*1000, color=color1, label='HOGR')
    t1 = df.index[1]
    t2 = df.index[-2]
        
    depo_list = ['MH33', 'MH34', 'MH35', 'MH40']
    label_list = ['MH33', 'MH34', 'MH35', 'MH40']
    col_list = [color2, 'C3', 'C1', color3]
    counter=0
    for depo in depo_list:
        vsensor = 'gps_differential__rtklib__daily'
        #vsensor = 'gps_differential__batch__daily'
        df = load_data(PATH_DATA + 'gnss_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
        df = df.groupby(pd.Grouper(freq=downsampling_freq)).aggregate(np.mean)
        
        # plot without HOGR correction
        ax[0].plot((df['e [m]']-df['e [m]'][-1])*1000, color=col_list[counter], label=label_list[counter])
        ax[1].plot((df['n [m]']-df['n [m]'][-1])*1000, color=col_list[counter], label=label_list[counter])
        ax[2].plot((df['h [m]']-df['h [m]'][-1])*1000, color=col_list[counter], label=label_list[counter])

        counter += 1
    
    ax[0].set_ylabel('Relative coordinates East [mm]')
    ax[1].set_ylabel('Relative coordinates North [mm]')
    ax[2].set_ylabel('Relative coordinates Elevation [mm]')

    xpos_ylabel = -0.07
    for i in range(3):
        ax[i].yaxis.set_label_coords(xpos_ylabel, 0.5)
        ax[i].xaxis.set_minor_locator(years)
        ax[i].xaxis.set_major_locator(biyearly)
        ax[i].axhline(y=0, xmin=0, xmax=1, color = 'gray', linestyle='dotted', linewidth=1)
        ax[i].legend(loc=2, ncol=5)
    
    date_format = mdates.DateFormatter('')
    ax[0].xaxis.set_major_formatter(date_format)
    date_format = mdates.DateFormatter('%Y')
    ax[2].xaxis.set_major_formatter(date_format)
    
    ax[0].set_ylim([-450, 150])
    ax[1].set_ylim([-50, 550])
    ax[2].set_ylim([-150, 450])
    ax[2].set_xlim([t1,t2])
    
    fname = 'fig_displacement_gps_MH.png'.format(vsensor, depo)
    plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,

    fig, ax = plt.subplots(2,1, figsize=(25/2.54, 25/2.54), facecolor='w', edgecolor='k', sharex=True)
    plt.subplots_adjust(left=0.09, right=0.995, bottom=0.1, top=0.99, hspace=0.05) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    
    ### Now the inclinometers
    #downsampling_freq = '1W' #weekly
    # downsampling_freq = '1440Min' #daily
    downsampling_freq = '10080Min' #weekly

    depo = 'HOGR'
    vsensor = 'gps_differential__rtklib__daily'
    #vsensor = 'gps_differential__batch__daily'
    df = load_data(PATH_DATA + 'gnss_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
    df  = df.groupby(pd.Grouper(freq=downsampling_freq)).aggregate(np.mean)        
    # ax[0].plot((df['e [m]']-df['e [m]'][-1])*1000, color=color1, label='HOGR')
    # ax[1].plot((df['n [m]']-df['n [m]'][-1])*1000, color=color1, label='HOGR')
    # ax[2].plot((df['h [m]']-df['h [m]'][-1])*1000, color=color1, label='HOGR')
    t1 = df.index[1]
    t2 = df.index[-2]
        
    depo_list = ['MH33', 'MH34', 'MH35']
    label_list = ['MH33', 'MH34', 'MH35']
    col_list = [color2, 'C3', 'C1', color3]
    counter=0
    for depo2 in depo_list:
        # depo2 = 'MH34'
        vsensor2 = 'gps_inclinometer'
        df = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo2, vsensor2, year=year_list, file_type='csv')
        df  = df.groupby(pd.Grouper(freq=downsampling_freq)).aggregate(np.mean)
        df = df.join(pd.DataFrame( -np.arcsin( np.sqrt( np.sin(df['inclinometer_north [°]']*np.pi/180).pow(2) + np.sin(df['inclinometer_west [°]']*np.pi/180).pow(2) ) )*180/math.pi, columns=['inclination']))
        df = df.join(pd.DataFrame(180/math.pi * np.arctan2(-df['inclinometer_north [°]'], -df['inclinometer_west [°]']), columns=['azimuth']))
        df.loc[df['azimuth'] < 0, 'azimuth'] = df['azimuth'] + 360
       
        # plot without HOGR correction
        ax[0].plot(df['inclination'] - df['inclination'][df['inclination'].notna()][-1], color=col_list[counter], label=label_list[counter])
        ax[1].plot(df['azimuth'] - df['azimuth'][df['azimuth'].notna()][-1], color=col_list[counter], label=label_list[counter])
        # ax[2].plot((df['h [m]']-df['h [m]'][-1])*1000, color=col_list[counter], label=label_list[counter])

        counter += 1
    
    ax[0].set_ylabel('Inclination [°]')
    ax[1].set_ylabel('Azimuth [°]')

    xpos_ylabel = -0.07
    for i in range(2):
        ax[i].yaxis.set_label_coords(xpos_ylabel, 0.5)
        ax[i].xaxis.set_minor_locator(years)
        ax[i].xaxis.set_major_locator(biyearly)
        ax[i].axhline(y=0, xmin=0, xmax=1, color = 'gray', linestyle='dotted', linewidth=1)
        ax[i].legend(loc=2, ncol=3)
    
    date_format = mdates.DateFormatter('')
    ax[0].xaxis.set_major_formatter(date_format)
    date_format = mdates.DateFormatter('%Y')
    ax[1].xaxis.set_major_formatter(date_format)
    
    # ax[0].set_ylim([-450, 150])
    # ax[1].set_ylim([-50, 550])
    # ax[2].set_ylim([-150, 450])
    ax[1].set_xlim([t1,t2])
    
    fname = 'fig_displacement_inclination_MH.png'.format(vsensor, depo)
    plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,


#%% MH03/NH18 fracture  displacement dx1 vs. time
if fig_displacement | ALL_FIG:
    # depo = 'MH03'
    depo_list = ['MH03', 'MH18']
    for depo in depo_list:
        fig, ax = plt.subplots(1,1, figsize=(25/2.54, 10/2.54), facecolor='w', edgecolor='k')
        plt.subplots_adjust(left=0.07, right=0.980, bottom=0.07, top=0.99) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2

        vsensor = 'displacement'
        df = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')

        ax.plot(df['displacement_dx1 [mm]'], color=color1)
        
        t1, t2 = dataavailability_flags(df['position []'])
        nodata_rectangles(ax, t1, t2)
        
        ax.xaxis.set_major_locator(biyearly)
        ax.xaxis.set_minor_locator(years)
        date_format = mdates.DateFormatter('%Y')
        ax.xaxis.set_major_formatter(date_format)
        ax.set_xlim([t2[0], t1[-1]])
        ax.set_ylabel('Fracture displacement [mm]')
        
        fname = 'fig_{:s}_{:s}.png'.format(vsensor, depo)
        plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,


#%% Fracture displacements vs. time
if fig_displacements_cryosphere | ALL_FIG:
    fig, ax = plt.subplots(1,1, figsize=(25/2.54, 7/2.54), facecolor='w', edgecolor='k')
    plt.subplots_adjust(left=0.07, right=0.980, bottom=0.10, top=0.95) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2

    # downsampling_freq = '1440Min'
    downsampling_freq = '10080Min'
    vsensor = 'displacement'

    depo = 'MH01'
    df1  = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
    df1  = df1.groupby(pd.Grouper(freq=downsampling_freq)).aggregate(np.mean)
    depo = 'MH02'
    df2  = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
    df2  = df2.groupby(pd.Grouper(freq=downsampling_freq)).aggregate(np.mean)
    depo = 'MH03'
    df3  = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
    df3  = df3.groupby(pd.Grouper(freq=downsampling_freq)).aggregate(np.mean)
    depo = 'MH04'
    df4  = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
    df4  = df4.groupby(pd.Grouper(freq=downsampling_freq)).aggregate(np.mean)
    depo = 'MH06'
    df6  = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
    df6  = df6.groupby(pd.Grouper(freq=downsampling_freq)).aggregate(np.mean)
    depo = 'MH08'
    df8  = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
    df8  = df8.groupby(pd.Grouper(freq=downsampling_freq)).aggregate(np.mean)
    depo = 'MH09'
    df9  = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
    df9  = df9.groupby(pd.Grouper(freq=downsampling_freq)).aggregate(np.mean)
    depo = 'MH18'
    df18 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
    df18 = df18.groupby(pd.Grouper(freq=downsampling_freq)).aggregate(np.mean)
    depo = 'MH20'
    df20 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
    df20 = df20.groupby(pd.Grouper(freq=downsampling_freq)).aggregate(np.mean)
    depo = 'MH21'
    df21 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
    df21 = df21.groupby(pd.Grouper(freq=downsampling_freq)).aggregate(np.mean)
    depo = 'MH22'
    df22 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
    df22 = df22.groupby(pd.Grouper(freq=downsampling_freq)).aggregate(np.mean)

    ax.plot(df1['displacement_dx1 [mm]'], label='MH01', color=color_list[1])
    ax.plot(df2['displacement_dx1 [mm]'], label='MH02', color=color_list[2])
    ax.plot(df3['displacement_dx1 [mm]'], label='MH03', color=color_list[3])
    ax.plot(df4['displacement_dx1 [mm]'], label='MH04', color=color_list[4])
    ax.plot(df6['displacement_dx1 [mm]'], label='MH06', color=color_list[5])
    ax.plot(df8['displacement_dx1 [mm]'], label='MH08', color=color_list[6])
    ax.plot(df9['displacement_dx1 [mm]'], label='MH09', color=color_list[7])
    ax.plot(df18['displacement_dx1 [mm]'], label='MH18', color=color_list[8])
    ax.plot(df20['displacement_dx1 [mm]'], label='MH20', color=color_list[9])
    ax.plot(df21['displacement_dx1 [mm]'], label='MH21', color=color_list[10])
    ax.plot(df22['displacement_dx1 [mm]'], label='MH22', color=color_list[11])
    
    t1, t2 = dataavailability_flags(df1['position []'])
    ax.legend(ncol=3, loc='upper left') # frameon=False, 
    ax.axhline(y=0, xmin=0, xmax=1, color = 'gray', linestyle='dotted', linewidth=1)
    ax.xaxis.set_major_locator(biyearly)
    ax.xaxis.set_minor_locator(years)
    date_format = mdates.DateFormatter('%Y')
    ax.xaxis.set_major_formatter(date_format)
    ax.set_xlim([t2[0], t1[-1]])
    ax.set_ylabel('Displacement horiz. [mm]')
    ax.set_ylim([-7,17])
    ax.set_yticks(range(-5,20,5))
    
    fname = 'fig_{:s}s_x.png'.format(vsensor)
    plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,

    fig, ax = plt.subplots(1,1, figsize=(25/2.54, 7/2.54), facecolor='w', edgecolor='k')
    plt.subplots_adjust(left=0.07, right=0.980, bottom=0.1, top=0.95) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2

    ax.plot(df2['displacement_dx2 [mm]'], label='MH02', color=color_list[2])
    ax.plot(df6['displacement_dx2 [mm]'], label='MH06', color=color_list[5])
    ax.plot(df8['displacement_dx2 [mm]'], label='MH08', color=color_list[6])
    ax.plot(df9['displacement_dx2 [mm]'], label='MH09', color=color_list[7])
    ax.plot(df20['displacement_dx2 [mm]'], label='MH20', color=color_list[9])
    ax.plot(df21['displacement_dx2 [mm]'], label='MH21', color=color_list[10])
    ax.plot(df22['displacement_dx2 [mm]'], label='MH22', color=color_list[11])
    
    t1, t2 = dataavailability_flags(df1['position []'])
    ax.legend(ncol=3, loc='upper left') # frameon=False, 
    ax.axhline(y=0, xmin=0, xmax=1, color = 'gray', linestyle='dotted', linewidth=1)
    ax.xaxis.set_major_locator(biyearly)
    ax.xaxis.set_minor_locator(years)
    date_format = mdates.DateFormatter('%Y')
    ax.xaxis.set_major_formatter(date_format)
    ax.set_xlim([t2[0], t1[-1]])
    ax.set_ylabel('Displacement vert. [mm]')
    ax.set_ylim([-7,22])
    ax.set_yticks(range(-10,30,5))
    
    fname = 'fig_{:s}s_y.png'.format(vsensor)
    plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,


#%% MH34 GPS displacement, inclinometer and MH08 
if fig_gps | ALL_FIG:
    depo1 = 'MH34'
    vsensor1 = 'gps_differential__rtklib__daily'
    df1 = load_data(PATH_DATA + 'gnss_derived_data_products/', depo1, vsensor1, year=year_list, file_type='csv')
    df1  = df1.groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)
    df1['3d [mm]'] = np.sqrt((df1['n [m]']-df1['n [m]'][0])**2 + (df1['e [m]']-df1['e [m]'][0])**2 + (df1['h [m]']-df1['h [m]'][0])**2) * 1000
    
    depo2 = 'MH34'
    vsensor2 = 'gps_inclinometer'
    df2 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo2, vsensor2, year=year_list, file_type='csv')
    df2  = df2.groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)
    df2 = df2.join(pd.DataFrame( -np.arcsin( np.sqrt( np.sin(df2['inclinometer_north [°]']*np.pi/180).pow(2) + np.sin(df2['inclinometer_west [°]']*np.pi/180).pow(2) ) )*180/math.pi, columns=['inclination']))
    df2 = df2.join(pd.DataFrame(180/math.pi * np.arctan2(-df2['inclinometer_north [°]'], -df2['inclinometer_west [°]']), columns=['azimuth']))

    depo3 = 'MH08'
    vsensor3 = 'displacement'
    df3 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo3, vsensor3, year=year_list, file_type='csv')
    df3 = df3[~np.isnan(df3['displacement_dx1 [mm]'])]
    df3  = df3.groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)

    depo = '{:s}_{:s}'.format(depo1,depo3)
    vsensor = '{:s}_{:s}'.format(vsensor1,vsensor3)
    df = pd.concat([df1, df2], axis=1, join='inner')
    #df['hydrological_year'] = (df.index + pd.DateOffset(months=3)).year
    
    
    #fig, ax1 = plt.subplots(1,1, figsize=(25/2.54, 10/2.54), facecolor='w', edgecolor='k')
    #plt.subplots_adjust(left=0.07, right=0.93, bottom=0.08, top=0.99) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    fig, ax1 = plt.subplots(2,1, figsize=(20/2.54, 14/2.54), facecolor='w', edgecolor='k')
    plt.subplots_adjust(left=0.14, right=0.84, bottom=0.09, top=0.99) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    
    ax1[0].plot(df1['3d [mm]'], color='C3')
    
    for tl in ax1[0].get_yticklabels():
        tl.set_color(color2)
    ax1[0].tick_params(axis='y', colors='C3')
    ax1[0].spines['left'].set_color('C3')
    
    ax1[0].set_ylabel('3D coordinates [mm]', color='C3')
    ax1[0].set_ylim([-25,725])
    ax1[0].set_yticks(range(0,700,100))
    
    ax1[0].xaxis.set_major_locator(biyearly)
    ax1[0].xaxis.set_minor_locator(years)
    date_format = mdates.DateFormatter('%Y')
    ax1[0].xaxis.set_major_formatter(date_format)
    ax1[0].set_xlim(df1['3d [mm]'].index[0], df1['3d [mm]'].index[-1])

    t1, t2 = dataavailability_flags(df1['position []'])
    nodata_rectangles(ax1[0], t1, t2)
       
    ax2 = ax1[0].twinx()
    ax2.plot(abs(df2['inclination'] - df2['inclination'][df2['inclination'].notna()][0]), color=color1, zorder=2) #, linewidth=1 #ax3.set_ylim([44,50])
       
    ax2.set_ylabel('Inclination [°]', color=color1)
    # ax2.set_ylim([-0.25,4.0])
    # ax2.set_yticks(np.arange(0,10.5,0.5))
    for tl in ax2.get_yticklabels():
        tl.set_color(color1)
    ax2.tick_params(axis='y', colors=color1)
    ax2.spines['right'].set_color(color1)

    ax2.xaxis.set_major_locator(biyearly)
    ax2.xaxis.set_minor_locator(years)
    date_format = mdates.DateFormatter('%Y')
    ax2.xaxis.set_major_formatter(date_format)
        
    ax1[1].plot(df3['displacement_dx1 [mm]'])
    ax1[1].set_ylabel('Displacement [mm]')

    ax1[1].xaxis.set_major_locator(biyearly)
    ax1[1].xaxis.set_minor_locator(years)
    ax1[1].xaxis.set_major_formatter(date_format)
    ax1[1].set_xlim(df1['3d [mm]'].index[0], df1['3d [mm]'].index[-1])

    fname = 'fig_{:s}_{:s}.png'.format(vsensor, depo)
    plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,


#%% MH34: gps displacement and inclinometer
if fig_gps | ALL_FIG:
    depo1 = 'MH34'
    vsensor1 = 'gps_differential__rtklib__daily'
    df1 = load_data(PATH_DATA + 'gnss_derived_data_products/', depo1, vsensor1, year=year_list, file_type='csv')
    df1  = df1.groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)
    df1['3d [mm]'] = np.sqrt((df1['n [m]']-df1['n [m]'][0])**2 + (df1['e [m]']-df1['e [m]'][0])**2 + (df1['h [m]']-df1['h [m]'][0])**2) * 1000
    
    depo2 = 'MH34'
    vsensor2 = 'gps_inclinometer'
    df2 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo2, vsensor2, year=year_list, file_type='csv')
    df2  = df2.groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)
    df2 = df2.join(pd.DataFrame( -np.arcsin( np.sqrt( np.sin(df2['inclinometer_north [°]']*np.pi/180).pow(2) + np.sin(df2['inclinometer_west [°]']*np.pi/180).pow(2) ) )*180/math.pi, columns=['inclination']))
    df2 = df2.join(pd.DataFrame(180/math.pi * np.arctan2(-df2['inclinometer_north [°]'], -df2['inclinometer_west [°]']), columns=['azimuth']))

    depo = '{:s}_{:s}'.format(depo1,depo2)
    vsensor = '{:s}_{:s}'.format(vsensor1,vsensor2)
    df = pd.concat([df1, df2], axis=1, join='inner')
    #df['hydrological_year'] = (df.index + pd.DateOffset(months=3)).year
        
    #fig, ax1 = plt.subplots(1,1, figsize=(25/2.54, 10/2.54), facecolor='w', edgecolor='k')
    #plt.subplots_adjust(left=0.07, right=0.93, bottom=0.08, top=0.99) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    fig, ax1 = plt.subplots(1,1, figsize=(12/2.54, 8/2.54), facecolor='w', edgecolor='k')
    plt.subplots_adjust(left=0.14, right=0.84, bottom=0.09, top=0.99) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    
    ax1.plot(df1['3d [mm]'], color='C3')
    
    # ax1.set_ylabel('Inclination (°)', color=color2)
    # ax1.set_ylim([-2,2])
    # ax1.set_yticks(np.arange(-2,2,0.4))
    for tl in ax1.get_yticklabels():
        tl.set_color('C3')
    ax1.tick_params(axis='y', colors='C3')
    ax1.spines['left'].set_color('C3')
    
    ax1.set_ylabel('Relative 3D coordinates [mm]', color='C3')
    ax1.set_ylim([-25,725])
    ax1.set_yticks(range(0,700,100))
    
    # ax1.xaxis.set_major_locator(biyearly)
    # ax1.xaxis.set_minor_locator(years)
    # ax1.xaxis.set_major_formatter(date_format)
    # ax1.set_xlim(df1['3d [mm]'].index[0], df1['3d [mm]'].index[-1])

    t1, t2 = dataavailability_flags(df1['position []'])
    nodata_rectangles(ax1, t1, t2)
       
    ax2 = ax1.twinx()
    ax2.plot(abs(df2['inclination'] - df2['inclination'][df2['inclination'].notna()][0]), color=color1, zorder=2) #, linewidth=1 #ax3.set_ylim([44,50])
       
    ax2.set_ylabel('Inclination [°]', color=color1)
    # ax2.set_ylim([-0.25,4.0])
    # ax2.set_yticks(np.arange(0,4.5,0.5))
    for tl in ax2.get_yticklabels():
        tl.set_color(color1)
    ax2.tick_params(axis='y', colors=color1)
    ax2.spines['right'].set_color(color1)

    ax2.xaxis.set_major_locator(biyearly)
    ax2.xaxis.set_minor_locator(years)
    ax2.xaxis.set_major_formatter(date_format)
    ax2.set_xlim(df1['3d [mm]'].index[0], df1['3d [mm]'].index[-1])

    fname = 'fig_{:s}_{:s}.png'.format(vsensor, depo)
    plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,


#%% MH10 resistivity rock
if fig_resistivity | ALL_FIG:
    depo = 'MH10'
    vsensor = 'resistivity_rock'
    df = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
    #df.rename(columns={'temperature_nearsurface_t2 [°C]':'temperature_nearsurface [°C]'}, inplace=True)
    
    fig, ax = plt.subplots(1,1, figsize=(25/2.54, 12/2.54), facecolor='w', edgecolor='k')
    plt.subplots_adjust(left=0.07, right=0.980, bottom=0.07, top=0.99) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    #fig, ax = plt.subplots(1,1, figsize=(12/2.54, 8/2.54), facecolor='w', edgecolor='k')
    #plt.subplots_adjust(left=0.13, right=0.995, bottom=0.08, top=0.99) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    ax.plot(df['resistivity_85cm [Mohm]'], color=color1)
#    ax.plot(df['resistivity_35_85cm [Mohm]'])
    
    t1, t2 = dataavailability_flags(df['position []'])
    nodata_rectangles(ax, t1, t2)
    
    ax.xaxis.set_major_locator(biyearly)
    ax.xaxis.set_minor_locator(years)
    date_format = mdates.DateFormatter('%Y')
    ax.xaxis.set_major_formatter(date_format)
    ax.set_xlim([t2[0], t1[-1]])
    ax.set_ylabel(r'Resistivity at 85cm [M$\Omega$]')
    ax.set_ylim([0,20])
    ax.set_yticks(range(0,20,2))
    
    fname = 'fig_{:s}_{:s}.png'.format(vsensor, depo)
    plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,


#%%
if fig_DRC | ALL_FIG:
## MH10/MH08 temperature rock (85cm) vs displacement (dx2)
    depo1 = 'MH10'
    vsensor1 = 'temperature_rock'
    df1 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo1, vsensor1, year=year_list, file_type='csv')
    df1 = df1[~np.isnan(df1['temperature_60cm [°C]'])]
    df1  = df1.groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)
    
    depo2 = 'MH08'
    vsensor2 = 'displacement'
    df2 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo2, vsensor2, year=year_list, file_type='csv')
    df2 = df2[~np.isnan(df2['displacement_dx1 [mm]'])]
    df2  = df2.groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)
    
    depo = '{:s}_{:s}'.format(depo1,depo2)
    vsensor = '{:s}_{:s}'.format(vsensor1,vsensor2)
    df = pd.concat([df1, df2], axis=1, join='inner')
    df['hydrological_year'] = (df.index + pd.DateOffset(months=3)).year
        
    fig, ax = plt.subplots(1,1, figsize=(12/2.54, 10/2.54), facecolor='w', edgecolor='k')
    plt.subplots_adjust(left=0.14, right=0.88, bottom=0.13, top=0.99) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    
    color_list_tmp = color_list.copy()
    year_list_tmp = year_list.copy()
    for i, y in enumerate(year_list):
        if len(df[df['hydrological_year'] == y]) == 0:
            color_list_tmp[i] = np.zeros(4)
            year_list_tmp[i] = ''
    
    for i, y in enumerate(year_list):#enumerate(np.unique(df['hydrological_year'])):
        df_y = df[df['hydrological_year'] == y]
        col_y = color_list[i] #y - year_list[0]
        ax.plot(df_y['temperature_85cm [°C]'], df_y['displacement_dx2 [mm]'],'.',color=col_y)
    
    ax.set_xlabel('Temperature rock [°C]')
    ax.set_ylabel('Fracture displacement [mm]')
    ax.yaxis.set_label_coords(xpos_ylabel_square, 0.5)
    ax.set_xlim([-12,12])
    ax.set_xticks(range(-12,16,4))

    colorbar_hydrologicalyears(color_list_tmp,year_list)
    
    fname = 'fig_{:s}_{:s}.png'.format(vsensor, depo)
    plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,
    
    
## MH10/MH42 temperature rock (85cm) vs relative position(3d)
    depo1 = 'MH10'
    vsensor1 = 'temperature_rock'
    df1 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo1, vsensor1, year=year_list, file_type='csv')
    df1  = df1 .groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)
    
    depo2 = 'HOGR'
    vsensor2 = 'gps_differential__rtklib__daily'
    df2 = load_data(PATH_DATA + 'gnss_derived_data_products/', depo2, vsensor2, year=year_list, file_type='csv')
    df2['3d [m]'] = np.sqrt((df2['n [m]']-df2['n [m]'][0])**2 + (df2['e [m]']-df2['e [m]'][0])**2 + (df2['h [m]']-df2['h [m]'][0])**2)
    df2  = df2 .groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)
    
    depo = '{:s}_{:s}'.format(depo1,depo2)
    vsensor = '{:s}_{:s}'.format(vsensor1,vsensor2)
    df = pd.concat([df1, df2], axis=1, join='inner')
    df['hydrological_year'] = (df.index + pd.DateOffset(months=3)).year

    fig, ax = plt.subplots(1,1, figsize=(12/2.54, 10/2.54), facecolor='w', edgecolor='k')
    plt.subplots_adjust(left=0.14, right=0.88, bottom=0.13, top=0.99) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    
    color_list_tmp = color_list.copy()
    year_list_tmp = year_list.copy()
    for i, y in enumerate(year_list):
        if len(df[df['hydrological_year'] == y]) == 0:
            color_list_tmp[i] = np.zeros(4)
            year_list_tmp[i] = ''

    for i, y in enumerate(year_list):
        df_y = df[df['hydrological_year'] == y]
        col_y = color_list[i]
        ax.plot(df_y['temperature_85cm [°C]'], df_y['3d [m]']*1000,'.', color=col_y)
    
    ax.set_xlabel('Temperature rock [°C]'.format(depo))
    ax.set_ylabel('Relative 3D coordinates [mm]'.format(depo))
    ax.yaxis.set_label_coords(xpos_ylabel_square, 0.5)
    ax.set_xlim([-12,12])
    ax.set_xticks(range(-12,16,4))
    # ax.set_ylim([-15,275])
    # ax.set_yticks(range(0,300,50))
    
    colorbar_hydrologicalyears(color_list_tmp,year_list)
    
    fname = 'fig_{:s}_{:s}.png'.format(vsensor, depo)
    plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,

## MH10/HOGR: temperature rock (85cm) vs relative position(3d)
    depo1 = 'MH10'
    vsensor1 = 'temperature_rock'
    df1 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo1, vsensor1, year=year_list, file_type='csv')
    df1  = df1 .groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)

    depo2 = 'HOGR'
    vsensor2 = 'gps_differential__rtklib__daily'
    df2 = load_data(PATH_DATA + 'gnss_derived_data_products/', depo2, vsensor2, year=year_list, file_type='csv')
    df2['3d [m]'] = np.sqrt((df2['n [m]']-df2['n [m]'][0])**2 + (df2['e [m]']-df2['e [m]'][0])**2 + (df2['h [m]']-df2['h [m]'][0])**2)
    df2  = df2 .groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)

    depo = '{:s}_{:s}'.format(depo1,depo2)
    vsensor = '{:s}_{:s}'.format(vsensor1,vsensor2)
    df = pd.concat([df1, df2], axis=1, join='inner')
    df['hydrological_year'] = (df.index + pd.DateOffset(months=3)).year

    fig, ax = plt.subplots(3,1, figsize=(12/2.54, 34/2.54), facecolor='w', edgecolor='k')
    plt.subplots_adjust(left=0.14, right=0.88, bottom=0.05, top=0.99,hspace=0.1) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2

    color_list_tmp = color_list.copy()
    year_list_tmp = year_list.copy()
    for i, y in enumerate(year_list):
        if len(df[df['hydrological_year'] == y]) == 0:
            color_list_tmp[i] = np.zeros(4)
            year_list_tmp[i] = ''

    for i, y in enumerate(year_list):
        df_y = df[df['hydrological_year'] == y]
        col_y = color_list[i]
        try:
            ax[0].plot(df_y['temperature_85cm [°C]'], (df_y['e [m]'] - df['e [m]'][0])*1000,'.', color=col_y)
            ax[1].plot(df_y['temperature_85cm [°C]'], (df_y['n [m]'] - df['n [m]'][0])*1000,'.', color=col_y)
            ax[2].plot(df_y['temperature_85cm [°C]'], (df_y['h [m]'] - df['h [m]'][0])*1000,'.', color=col_y)
        except:
            pass
    #
    ax[2].set_xlabel('Temperature rock [°C]'.format(depo))
    ax[2].set_xlim([-12,12])
    ax[2].set_xticks(range(-12,16,4))
    ax[0].set_ylabel('Relative E-W change [mm]'.format(depo))
    ax[1].set_ylabel('Relative N-S change [mm]'.format(depo))
    ax[2].set_ylabel('Relative elevation change [mm]'.format(depo))
    #ax.yaxis.set_label_coords(xpos_ylabel_square, 0.5)

    #ax[0].set_ylim([-25,25])
    #ax.set_yticks(range(0,210,30))
    #
    xpos_ylabel = -0.14
    for i in range(3):
        ax[i].yaxis.set_label_coords(xpos_ylabel, 0.5)
        plt.sca(ax[i])
        colorbar_hydrologicalyears(color_list_tmp,year_list)
    #
    fname = 'fig_{:s}_{:s}_channelwise.png'.format(vsensor, depo)
    plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,


## MH10/MH34: temperature rock (85cm) and GPS displacement
    depo1 = 'MH10'
    vsensor1 = 'temperature_rock'
    df1 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo1, vsensor1, year=year_list, file_type='csv')
    df1 = df1[~np.isnan(df1['temperature_60cm [°C]'])]
    df1  = df1.groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)
    
    depo2 = 'MH34'
    vsensor2 = 'gps_differential__rtklib__daily'
    df2 = load_data(PATH_DATA + 'gnss_derived_data_products/', depo2, vsensor2, year=year_list, file_type='csv')
    df2['3d [mm]'] = np.sqrt((df2['n [m]']-df2['n [m]'][0])**2 + (df2['e [m]']-df2['e [m]'][0])**2 + (df2['h [m]']-df2['h [m]'][0])**2) * 1000
    df2  = df2.groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)
    
    depo = '{:s}_{:s}'.format(depo1,depo2)
    vsensor = '{:s}_{:s}'.format(vsensor1,vsensor2)
    df = pd.concat([df1, df2], axis=1, join='inner')
    df['hydrological_year'] = (df.index + pd.DateOffset(months=3)).year
    
    fig, ax = plt.subplots(1,1, figsize=(12/2.54, 10/2.54), facecolor='w', edgecolor='k')
    plt.subplots_adjust(left=0.14, right=0.88, bottom=0.13, top=0.99) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    
    color_list_tmp = color_list.copy()
    year_list_tmp = year_list.copy()
    for i, y in enumerate(year_list):
        if len(df[df['hydrological_year'] == y]) == 0:
            color_list_tmp[i] = np.zeros(4)
            year_list_tmp[i] = ''
    
    for i, y in enumerate(year_list):#enumerate(np.unique(df['hydrological_year'])):
        df_y = df[df['hydrological_year'] == y]
        col_y = color_list[i] #y - year_list[0]
        try:
            ax.plot(df_y['temperature_85cm [°C]'], df_y['3d [mm]'],'.',color=col_y)
        except:
            pass
    
    ax.set_xlabel('Temperature rock [°C]')
    ax.set_ylabel('Relative 3D coordinates [mm]')
    ax.yaxis.set_label_coords(xpos_ylabel_square, 0.5)
    ax.set_xlim([-12,12])
    ax.set_xticks(range(-12,16,4))
    
    colorbar_hydrologicalyears(color_list_tmp,year_list)
    
    fname = 'fig_{:s}_{:s}.png'.format(vsensor, depo)
    plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,

## MH10/MH34: temperature rock (85cm) and inclinometer
    fig, ax = plt.subplots(1,1, figsize=(12/2.54, 10/2.54), facecolor='w', edgecolor='k')
    plt.subplots_adjust(left=0.14, right=0.88, bottom=0.13, top=0.99) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    
    depo1 = 'MH10'
    vsensor1 = 'temperature_rock'
    df1 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo1, vsensor1, year=year_list, file_type='csv')
    df1 = df1[~np.isnan(df1['temperature_60cm [°C]'])]
    df1  = df1.groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)
    
    depo2 = 'MH34'
    vsensor2 = 'gps_inclinometer'
    df2 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo2, vsensor2, year=year_list, file_type='csv')
    df2  = df2.groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)
    df2 = df2.join(pd.DataFrame( -np.arcsin( np.sqrt( np.sin(df2['inclinometer_north [°]']*np.pi/180).pow(2) + np.sin(df2['inclinometer_west [°]']*np.pi/180).pow(2) ) )*180/math.pi, columns=['inclination']))
    df2 = df2.join(pd.DataFrame(180/math.pi * np.arctan2(-df2['inclinometer_north [°]'], -df2['inclinometer_west [°]']), columns=['azimuth']))

    
    depo = '{:s}_{:s}'.format(depo1,depo2)
    vsensor = '{:s}_{:s}'.format(vsensor1,vsensor2)
    df = pd.concat([df1, df2], axis=1, join='inner')
    df['hydrological_year'] = (df.index + pd.DateOffset(months=3)).year
        
    color_list_tmp = color_list.copy()
    year_list_tmp = year_list.copy()
    for i, y in enumerate(year_list):
        if len(df[df['hydrological_year'] == y]) == 0:
            color_list_tmp[i] = np.zeros(4)
            year_list_tmp[i] = ''
    
    for i, y in enumerate(year_list):#enumerate(np.unique(df['hydrological_year'])):
        df_y = df[df['hydrological_year'] == y]
        col_y = color_list[i] #y - year_list[0]
        # print(-np.arcsin( np.sqrt( np.sin(df_y['inclinometer_north [°]']*np.pi/180).pow(2) + np.sin(df_y['inclinometer_west [°]']*np.pi/180).pow(2) ) )*180/math.pi)
        try:
            # ax.plot(df_y['temperature_85cm [°C]'], np.arcsin( np.sqrt( np.sin(df_y['inclinometer_north [°]']*np.pi/180).pow(2) + np.sin(df_y['inclinometer_west [°]']*np.pi/180).pow(2) ) )*180/math.pi,'.',color=col_y)
            ax.plot(df_y['temperature_85cm [°C]'], abs(df_y['inclination'] - df['inclination'][df['inclination'].notna()][0]),'.',color=col_y)
        except:
            pass
    
    ax.set_xlabel('Temperature rock [°C]')
    ax.set_ylabel('Inclination [°]')
    ax.yaxis.set_label_coords(xpos_ylabel_square, 0.5)
    ax.set_xlim([-12,12])
    ax.set_xticks(range(-12,16,4))
    
    colorbar_hydrologicalyears(color_list_tmp,year_list)
    
    fname = 'fig_{:s}_{:s}.png'.format(vsensor, depo)
    plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,


#%% Rock temperatures
if fig_rocktemperature | ALL_FIG:
    downsampling_freq = '1440Min'
    # print(downsampling_freq)
    # depo = 'MH10'
    depo_list = ['MH10', 'MH11', 'MH27', 'MH30', 'MH46', 'MH47']
    for depo in depo_list:
        print("")
        print(depo)
        vsensor = 'temperature_rock'
        df = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
        df  = df.groupby(pd.Grouper(freq=downsampling_freq)).aggregate(np.mean)

        fig, ax = plt.subplots(1,1, figsize=(25/2.54, 12/2.54), facecolor='w', edgecolor='k')
        plt.subplots_adjust(left=0.07, right=0.980, bottom=0.08, top=0.89) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2

        t1, t2 = dataavailability_flags(df['position []'])
        nodata_rectangles(ax, t1, t2)
        
        ax.axhline(y=0, xmin=0, xmax=1, color = 'gray', linestyle='dotted', linewidth=1)

        # ax.plot(df['temperature_nearsurface_t2 [°C]'], label='near surface')
        # ax.plot(df['temperature_5cm [°C]'], label='5 cm')
        ax.plot(df['temperature_10cm [°C]'], label='10 cm', alpha=0.8)
        # ax.plot(df['temperature_20cm [°C]'], label='20 cm')
        # ax.plot(df['temperature_30cm [°C]'], label='30 cm')
        # ax.plot(df['temperature_35cm [°C]'], label='35 cm')
        # ax.plot(df['temperature_50cm [°C]'], label='50 cm')
        # ax.plot(df['temperature_60cm [°C]'], label='60 cm')

        try:
            ax.plot(df['temperature_85cm [°C]'], label='85 cm', alpha=0.8)
        except:
            pass
        try:
            ax.plot(df['temperature_100cm [°C]'], label='100 cm', alpha=0.8)
        except:
            pass

        # x = np.arange(df['temperature_10cm [°C]'].dropna().index.size)
        # fit = np.polyfit(x, df['temperature_10cm [°C]'].dropna().values, deg=1)
        # print ("Slope : " + str(fit[0]) + str(fit[0]*365))
        # print ("Intercept : " + str(fit[1]))
        # fit_fn = np.poly1d(fit)
        # ax.plot(df['temperature_10cm [°C]'].dropna().index, fit_fn(x), color='tab:blue', linestyle='dotted')
        # print(fit_fn(x))

        # try:
        #     x = np.arange(df['temperature_85cm [°C]'].dropna().index.size)
        #     fit = np.polyfit(x, df['temperature_85cm [°C]'].dropna().values, deg=1)
        #     print ("Slope : " + str(fit[0]*365*10))
        #     print ("Intercept : " + str(fit[1]))
        #     fit_fn = np.poly1d(fit)
        #     ax.plot(df['temperature_85cm [°C]'].dropna().index, fit_fn(x), color='tab:orange', linestyle='dotted')
        #     # print(fit_fn(x))
        # except:
        #     pass
        # try:
        #     x = np.arange(df['temperature_100cm [°C]'].dropna().index.size)
        #     fit = np.polyfit(x, df['temperature_100cm [°C]'].dropna().values, deg=1)
        #     print ("Slope : " + str(fit[0]*365*10))
        #     print ("Intercept : " + str(fit[1]))
        #     fit_fn = np.poly1d(fit)
        #     ax.plot(df['temperature_100cm [°C]'].dropna().index, fit_fn(x), color='tab:orange', linestyle='dotted')
        # except:
        #     pass

        # # Mean lines
        # try:
        #     mgt = np.mean(df['temperature_85cm [°C]'])
        #     # print(mgt)
        #     ax.axhline(y=mgt, xmin=0, xmax=1, color = 'darkgrey', linestyle='dotted', label='mean 85cm') #linewidth=1)
        # except:
        #     mgt = np.mean(df['temperature_100cm [°C]'])
        #     # print(mgt)
        #     ax.axhline(y=mgt, xmin=0, xmax=1, color = 'darkgrey', linestyle='dotted', label='mean 100cm') #linewidth=1)

        try:
            counter = 0
            # magt2 = pd.DataFrame(columns=('year', 'MAGT'))
            df.index = df.index + pd.DateOffset(months=3)
            for y in df.index.year.unique():
                df_y = df.loc[df.index.year == y,:]
                df_y_int = df_y.interpolate(method='linear')
                if df_y['temperature_85cm [°C]'].count() > 250 :
                    magt = np.mean(df_y_int['temperature_85cm [°C]'])
                    # magt2.loc[counter] = [y, np.mean(df_y_int['temperature_85cm [°C]'])]
                    if counter == 0:
                        ax.plot([dt.datetime(y-1,10,1), dt.datetime(y,10,1)], [magt, magt], color='darkred', label='MAGT 85cm')
                    else:
                        ax.plot([dt.datetime(y-1,10,1), dt.datetime(y,10,1)], [magt, magt], color='darkred')
                    counter += 1
                    print(y, magt)
                else:
                    print(y)

            # x = np.arange(magt2.index.size)
            # fit = np.polyfit(x, magt2['MAGT'].dropna().values, deg=1)
            # print ("Slope : " + str(fit[0]))
            # print ("Intercept : " + str(fit[1]))
            # fit_fn = np.poly1d(fit)
            # # print(dt.datetime(int(magt2['year'].min()-1),10,1))
            # # print(fit_fn(x)[0])
            # # print(dt.datetime(int(magt2['year'].max()),10,1))
            # # print(fit_fn(x)[-1])
            # ax.plot([dt.datetime(int(magt2['year'].min()-1),10,1), dt.datetime(int(magt2['year'].max()),10,1)], [fit_fn(x)[0], fit_fn(x)[-1]], color='red', linewidth=0.5)

        except:
            pass
        

        if depo != 'MH10':
            counter = 0
            # magt2 = pd.DataFrame(columns=('year', 'MAGT'))
            df.index = df.index + pd.DateOffset(months=0)
            for y in df.index.year.unique():
                df_y = df.loc[df.index.year == y,:]
                df_y_int = df_y.interpolate(method='linear')
                if df_y['temperature_100cm [°C]'].count() > 250 :
                    magt = np.mean(df_y_int['temperature_100cm [°C]'])
                    # magt2.loc[counter] = [y, np.mean(df_y_int['temperature_100cm [°C]'])]
                    if counter == 0:
                        ax.plot([dt.datetime(y-1,10,1), dt.datetime(y,10,1)], [magt, magt], color='darkred', label='MAGT 100cm')
                    else:
                        ax.plot([dt.datetime(y-1,10,1), dt.datetime(y,10,1)], [magt, magt], color='darkred')
                    counter += 1
                    print(y,magt)
                else:
                    print(y)

            # x = np.arange(magt2.index.size)
            # fit = np.polyfit(x, magt2['MAGT'].dropna().values, deg=1)
            # print ("Slope : " + str(fit[0]))
            # print ("Intercept : " + str(fit[1]))
            # fit_fn = np.poly1d(fit)
            # # print(dt.datetime(int(magt2['year'].min()-1),10,1))
            # # print(fit_fn(x)[0])
            # # print(dt.datetime(int(magt2['year'].max()),10,1))
            # # print(fit_fn(x)[-1])
            # ax.plot([dt.datetime(int(magt2['year'].min()-1),10,1), dt.datetime(int(magt2['year'].max()),10,1)], [fit_fn(x)[0], fit_fn(x)[-1]], color='red', linewidth=0.5)

        ax.xaxis.set_major_locator(biyearly)
        ax.xaxis.set_minor_locator(years)
        ax.xaxis.set_major_formatter(date_format)
        date_format = mdates.DateFormatter('%Y')
        ax.xaxis.set_major_formatter(date_format)
        ax.set_xlim([t2[0], t1[-1]])
        ax.set_ylabel('Temperature rock [°C]')
        ax.set_ylim([-20,20])
        ax.set_yticks(range(-20,30,10))
        ax.legend(ncol=5, loc='lower right') # frameon=False, 
        
        fname = 'fig_{:s}_{:s}.png'.format(vsensor, depo)
        plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,


    ## MH10 annual ground temperature plots
    # depo = 'MH10'
    depo_list = ['MH10', 'MH11', 'MH27', 'MH30', 'MH46', 'MH47']
    for depo in depo_list:
        vsensor = 'temperature_rock'
        df = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
        df = df.groupby(pd.Grouper(freq='1D')).aggregate(np.mean)
        df_m = df.groupby(pd.Grouper(freq='1M')).aggregate(np.mean)

        df['hydrological_year'] = (df.index + pd.DateOffset(months=3)).year
        df['hydrological_month'] = (df.index + pd.DateOffset(months=3)).month
        df['year'] = df.index.year
        df['month'] = df.index.month

        df_m['hydrological_year'] = (df_m.index + pd.DateOffset(months=3)).year
        df_m['hydrological_month'] = (df_m.index + pd.DateOffset(months=3)).month
        df_m['year'] = df_m.index.year
        df_m['month'] = df_m.index.month

        fig, ax = plt.subplots(1,1, figsize=(34/2.54, 12/2.54), facecolor='w', edgecolor='k')
        plt.subplots_adjust(left=0.14, right=0.88, bottom=0.15, top=0.89,hspace=0.1) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2

        color_list_tmp = color_list.copy()
        year_list_tmp = year_list.copy()

        # colorbar_hydrologicalyears(color_list,year_list)
        # print(colorbar_hydrologicalyears)
        # print(df_m)

        # ax.plot(df['month'], df['temperature_85cm [°C]'], color=df['year'].apply(lambda x: color_list[x]))
        import seaborn as sns

        # palette = sns.color_palette()
        # f_ax1 = plt.subplot(1, 3, 1)
        if 'temperature_85cm [°C]' in df.columns:
            try: 
                sns.lineplot(x=df['hydrological_month'], y=df['temperature_85cm [°C]'], hue=df['hydrological_year'], legend=False)
            except:
                pass
        try: 
            sns.lineplot(x=df['hydrological_month'], y=df['temperature_100cm [°C]'], hue=df['hydrological_year'], legend=False)
        except:
            pass
        
        # print(dt.datetime.today().year)
        df_m = df_m[df_m['hydrological_year'] == dt.datetime.today().year]
        # print(df_m)
        if 'temperature_85cm [°C]' in df.columns:
            try: 
                sns.lineplot(x=df_m['hydrological_month'], y=df_m['temperature_85cm [°C]'], linewidth = 3)
            except:
                pass
        try: 
            sns.lineplot(x=df_m['hydrological_month'], y=df_m['temperature_100cm [°C]'], linewidth = 3)
        except:
            pass

        ax.axhline(y=0, xmin=0, xmax=1, color = 'gray', linestyle='dotted', linewidth=1)

        ax.set_title('Ground Surface Temperature - {:s}'.format(depo))
        ax.set_xlabel('Month')
        ax.set_ylabel('Temperature rock [°C]')
        ax.set_xticks(range(1,13,1))
        ax.set_xticklabels(['10','11','12','1','2','3','4','5','6','7','8','9'])
        ax.set_ylim([-15,10])
        ax.set_yticks(range(-15,15,5))


        # ax.set_xlabel('Temperature rock [°C]'.format(depo))
        # ax.set_xlim([-12,12])
        # ax.set_xticks(range(-12,16,4))
        # ax.set_ylabel('Relative N-S change (mm)'.format(depo))
        #
        fname = 'fig_{:s}_{:s}_yearly.png'.format(vsensor, depo)
        plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,


#%%
if fig_DRC | ALL_FIG:
    ## MH10/MH10: temperature rock VS resistivity
    depo = 'MH10'
    vsensor1 = 'temperature_rock'
    df1 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor1, year=year_list, file_type='csv')
    df1  = df1.groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)
    
    vsensor2 = 'resistivity_rock'
    df2 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor2, year=year_list, file_type='csv')
    df2  = df2.groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)
    
    vsensor = '{:s}_{:s}'.format(vsensor1,vsensor2)
    df = pd.concat([df1, df2], axis=1, join='inner')
    df['hydrological_year'] = (df.index + pd.DateOffset(months=3)).year
    
    
    fig, ax = plt.subplots(1,1, figsize=(12/2.54, 10/2.54), facecolor='w', edgecolor='k')
    plt.subplots_adjust(left=0.14, right=0.88, bottom=0.13, top=0.99) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    
    color_list_tmp = color_list.copy()
    year_list_tmp = year_list.copy()
    for i, y in enumerate(year_list):
        if len(df[df['hydrological_year'] == y]) == 0:
            color_list_tmp[i] = np.zeros(4)
            year_list_tmp[i] = ''
            
    for i, y in enumerate(year_list):
        df_y = df[df['hydrological_year'] == y]
        col_y = color_list[i]
        ax.plot(df_y['temperature_85cm [°C]'], df_y['resistivity_85cm [Mohm]'],'.',color=col_y)
    
    ax.set_xlabel('Temperature rock [°C]'.format(depo))
    ax.set_ylabel(r'Resistivity rock [M$\Omega$]')
    #xpos_ylabel_square = -0.15
    ax.yaxis.set_label_coords(xpos_ylabel_square, 0.5)
    ax.set_xlim([-12,12])
    ax.set_xticks(range(-12,16,4))
    ax.set_ylim([0,19])
    ax.set_yticks(range(0,20,2))
    colorbar_hydrologicalyears(color_list_tmp,year_list)
    
    fname = 'fig_{:s}_{:s}.png'.format(vsensor, depo)
    plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,
    

#%% MH10/11/12: rock temperature
if fig_rocktemperature_cryosphere | ALL_FIG:
    # downsampling_freq = '1440Min'
    downsampling_freq = '10080Min'
    # print(downsampling_freq)
    vsensor = 'temperature_rock'
    depo = 'MH10'
    df1 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
    df1  = df1.groupby(pd.Grouper(freq=downsampling_freq)).aggregate(np.mean)
    depo = 'MH11'
    df2 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
    df2  = df2.groupby(pd.Grouper(freq=downsampling_freq)).aggregate(np.mean)
    depo = 'MH12'
    df3 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
    df3  = df3.groupby(pd.Grouper(freq=downsampling_freq)).aggregate(np.mean)

    fig, ax = plt.subplots(1,1, figsize=(25/2.54, 7/2.54), facecolor='w', edgecolor='k')
    plt.subplots_adjust(left=0.07, right=0.980, bottom=0.1, top=0.99) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    ax.plot(df1['temperature_85cm [°C]'], label='MH10 - south exposed', color='red')
    ax.plot(df2['temperature_85cm [°C]'], label='MH11 - north exposed', color='blue')
    ax.plot(df2['temperature_100cm [°C]'], color='blue')
    ax.plot(df3['temperature_85cm [°C]'], label='MH12 - east exposed', color='grey')
    
    t1, t2 = dataavailability_flags(df1['position []'])
    # nodata_rectangles(ax, t1, t2)
    ax.axhline(y=0, xmin=0, xmax=1, color = 'gray', linestyle='dotted', linewidth=1)
    ax.legend(ncol=3, loc='upper left') # frameon=False, 
    
    ax.xaxis.set_major_locator(biyearly)
    ax.xaxis.set_minor_locator(years)
    ax.xaxis.set_major_formatter(date_format)
    #ax.xaxis.set_major_locator(years)
    date_format = mdates.DateFormatter('%Y')
    ax.xaxis.set_major_formatter(date_format)
    ax.set_xlim([t2[0], t1[-1]])
    ax.set_ylabel('Temperature rock [°C]')
    ax.set_ylim([-20,15])
    ax.set_yticks(range(-15,15,5))
    
    fname = 'fig_{:s}s.png'.format(vsensor)
    plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,








#%% MH25/MH15: air temperature and radiation
if fig_weatherstation | ALL_FIG:
    ## MH25 annual air temperature plots
    #depo = 'MH25'
    depo_list = ['MH25', 'MH51']
    for depo in depo_list:
        vsensor = 'vaisalawxt520windpth'
        df = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
        df = df.groupby(pd.Grouper(freq='1D')).aggregate(np.mean)
        df_m = df.groupby(pd.Grouper(freq='1M')).aggregate(np.mean)

        df['hydrological_year'] = (df.index + pd.DateOffset(months=3)).year
        df['hydrological_month'] = (df.index + pd.DateOffset(months=3)).month
        df['year'] = df.index.year
        df['month'] = df.index.month
        df['day'] = df.index.day
        
        df_m['hydrological_year'] = (df_m.index + pd.DateOffset(months=3)).year
        df_m['hydrological_month'] = (df_m.index + pd.DateOffset(months=3)).month
        df_m['year'] = df_m.index.year
        df_m['month'] = df_m.index.month

        fig, ax = plt.subplots(1,1, figsize=(34/2.54, 12/2.54), facecolor='w', edgecolor='k')
        plt.subplots_adjust(left=0.14, right=0.88, bottom=0.15, top=0.89,hspace=0.1) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2

        # palette = sns.color_palette()
        # f_ax1 = plt.subplot(1, 3, 1)
        sns.lineplot(x=df['hydrological_month'], y=df['temp_air [°C]'], hue=df['hydrological_year'], legend=False)
        
        # print(dt.datetime.today().year)
        df_m = df_m[df_m['hydrological_year'] == dt.datetime.today().year]
        # print(df_m)
        sns.lineplot(x=df_m['hydrological_month'], y=df_m['temp_air [°C]'], linewidth = 3, legend='full')
        
        ax.axhline(y=0, xmin=0, xmax=1, color = 'gray', linestyle='dotted', linewidth=1)
        ax.set_title('Air Temperature Matterhorn 2010 - 2021')
        ax.set_xlabel('Month')
        ax.set_ylabel('Air Temperature [°C]')
        ax.set_xticks(range(1,13,1))
        ax.set_xticklabels(['10','11','12','1','2','3','4','5','6','7','8','9'])

        fname = 'fig_{:s}_{:s}_yearly.png'.format(vsensor, depo)
        plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,


    downsampling_freq = '10080Min'

    depo1 = 'MH25'
    vsensor1 = 'vaisalawxt520windpth'
    df1 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo1, vsensor1, year=year_list, file_type='csv')
    df1  = df1.groupby(pd.Grouper(freq=downsampling_freq)).aggregate(np.mean)
    
    depo2 = 'MH15'
    vsensor2 = 'radiometer__conv'
    df2 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo2, vsensor2, year=year_list, file_type='csv')
    df2  = df2.groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)

    depo3 = 'MH51'
    vsensor3 = 'vaisalawxt520windpth'
    df3 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo3, vsensor3, year=year_list, file_type='csv')
    df3 = df3.groupby(pd.Grouper(freq=downsampling_freq)).aggregate(np.mean)
    
    depo = '{:s}_{:s}'.format(depo1,depo2,depo3)
    vsensor = '{:s}_{:s}'.format(vsensor1,vsensor2,vsensor3)
    df = pd.concat([df1, df2, df3], axis=1, join='inner')
    #df['hydrological_year'] = (df.index + pd.DateOffset(months=3)).year
        
    fig, ax1 = plt.subplots(1,1, figsize=(26/2.54, 10/2.54), facecolor='w', edgecolor='k')
    plt.subplots_adjust(left=0.07, right=0.92, bottom=0.08, top=0.99) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    #fig, ax1 = plt.subplots(1,1, figsize=(12/2.54, 8/2.54), facecolor='w', edgecolor='k')
    #plt.subplots_adjust(left=0.15, right=0.85, bottom=0.08, top=0.99) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    
    ax1.plot(df1['temp_air [°C]'], color=color1, label='MH25')
    ax1.plot(df3['temp_air [°C]'], color=color3, label='MH51')
    t1, t2 = dataavailability_flags(df1['temp_air [°C]'])
    # nodata_rectangles(ax1, t1, t2)

    print("")
    print(depo1)
    counter = 0
    # magt2 = pd.DataFrame(columns=('year', 'MAGT'))
    df1.index = df1.index + pd.DateOffset(months=0)
    for y in df1.index.year.unique():
        df1_y = df1.loc[df1.index.year == y,:]
        # print(df1_y)
        df1_y_int = df1_y.interpolate(method='linear')
        if df1_y['temp_air [°C]'].count() > 40 :
            maat = np.mean(df1_y_int['temp_air [°C]'])
            # magt2.loc[counter] = [y, np.mean(df_y_int['temperature_100cm [°C]'])]
            # if counter == 0:
            #     ax1.plot([dt.datetime(y-1,10,1), dt.datetime(y,10,1)], [maat, maat], color='darkred', label='MAAT')
            # else:
            ax1.plot([dt.datetime(y-1,10,1), dt.datetime(y,10,1)], [maat, maat], color=color1)
            counter += 1
            print(y,maat)
        else:
            print(y)

    print("")
    print(depo3)
    counter = 0
    counter = 0
    df3.index = df3.index + pd.DateOffset(months=0)
    for y in df3.index.year.unique():
        df3_y = df3.loc[df3.index.year == y,:]
        df3_y_int = df3_y.interpolate(method='linear')
        if df3_y['temp_air [°C]'].count() > 40 :
            maat = np.mean(df3_y_int['temp_air [°C]'])
            ax1.plot([dt.datetime(y-1,10,1), dt.datetime(y,10,1)], [maat, maat], color=color3)
            counter += 1
            print(y,maat)
        else:
            print(y)


    ### fitted trends
    # x = np.arange(df1['temp_air [°C]'].dropna().index.size)
    # fit = np.polyfit(x, df1['temp_air [°C]'].dropna().values, deg=1)
    # print ("Slope : " + str(fit[0]) + str(fit[0]*365))
    # print ("Intercept : " + str(fit[1]))
    # fit_fn = np.poly1d(fit)
    # ax1.plot(df1['temp_air [°C]'].dropna().index, fit_fn(x), color=color1, linewidth=0.5)

    # x = np.arange(df3['temp_air [°C]'].dropna().index.size)
    # fit = np.polyfit(x, df3['temp_air [°C]'].dropna().values, deg=1)
    # print ("Slope : " + str(fit[0]) + str(fit[0]*365))
    # print ("Intercept : " + str(fit[1]))
    # fit_fn = np.poly1d(fit)
    # ax1.plot(df3['temp_air [°C]'].dropna().index, fit_fn(x), color=color3, linewidth=0.5)

    # counter = 0
    # # print(df.index.year.unique())
    # for y in df1.index.year.unique():
    #     df1_y = df1.loc[df1.index.year == y,:]
    #     df1_y_int = df1_y.interpolate(method='linear')
    #     # print(df1_y)
    #     if np.sum(~np.isnan(df1_y_int['temp_air [°C]'].values)) > 50 :
    #         magt = np.mean(df1_y_int['temp_air [°C]'])
    #         # print(magt)
    #         if counter == 0:
    #             ax1.plot([dt.datetime(y,1,1), dt.datetime(y+1,1,1)], [magt, magt], color='darkgrey', linestyle='dotted', label='MAAT MH25')
    #             counter += 1
    #         else:
    #             ax1.plot([dt.datetime(y,1,1), dt.datetime(y+1,1,1)], [magt, magt], color='darkgrey', linestyle='dotted')

    # counter = 0
    # df1.index = df1.index + pd.DateOffset(months=4)
    # for y in df1.index.year.unique():
    #     df1_y = df1.loc[df1.index.year == y,:]
    #     df1_y_int = df1_y.interpolate(method='linear')
    #     # print(df1_y)
    #     if np.sum(~np.isnan(df1_y_int['temp_air [°C]'].values)) > 50 :
    #         magt = np.mean(df1_y_int['temp_air [°C]'])
    #         # print(magt)
    #         if counter == 0:
    #             ax1.plot([dt.datetime(y-1,10,1), dt.datetime(y,10,1)], [magt, magt], color='darkgrey', linestyle='dotted', label='MAAT hydr MH25')
    #             counter += 1
    #         else:
    #             ax1.plot([dt.datetime(y-1,10,1), dt.datetime(y,10,1)], [magt, magt], color='darkgrey', linestyle='dotted')


    ax2 = ax1.twinx()
    ax2.plot(df2['net_radiation [Wm^-2]'], color=color2, alpha=0.3, label='MH15') #, linewidth=1 #ax3.set_ylim([44,50])

    ax1.legend(loc="lower left")
    ax2.legend(loc="lower center")

    ax1.set_ylabel('Air temperature [°C]')
    ax1.set_ylim([-45, 15])
    ax1.set_yticks(range(-30,20,10))
    ax1.axhline(y=0, xmin=0, xmax=1, color = 'gray', linestyle='dotted', linewidth=1)


    ax1.xaxis.set_major_locator(biyearly)
    ax1.xaxis.set_minor_locator(years)
    ax1.set_xlim([t2[0], t1[-1]])
    date_format = mdates.DateFormatter('%Y')
    ax1.xaxis.set_major_formatter(date_format)

    ax2.set_ylabel(r'Total Net Radiation [Wm$^{-2}$]', color=color2)
    ax2.set_ylim([-100, 900])
    ax2.set_yticks(range(-100,400,100))
    
    for tl in ax2.get_yticklabels():
        tl.set_color(color2)
    ax2.tick_params(axis='y', colors=color2)
    ax2.spines['right'].set_color(color2)
    
    fname = 'fig_{:s}_{:s}.png'.format(vsensor, depo)
    plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,




# #%% HOGR: gps (displacement and inclinometer)
# if fig_gps | ALL_FIG:
#     depo1 = 'HOGR'
#     vsensor1 = 'gps_differential__rtklib__daily'
#     df1 = load_data(PATH_DATA + 'gnss_derived_data_products/', depo1, vsensor1, year=year_list, file_type='csv')
#     df1  = df1.groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)
#     df1['3d [mm]'] = np.sqrt((df1['n [m]']-df1['n [m]'][0])**2 + (df1['e [m]']-df1['e [m]'][0])**2 + (df1['h [m]']-df1['h [m]'][0])**2) * 1000
#     df1['e [mm]'] = np.sqrt((df1['e [m]']-df1['e [m]'][0])**2) * 1000

#     depo3 = 'MH08'
#     vsensor3 = 'displacement'
#     df3 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo3, vsensor3, year=year_list, file_type='csv')
#     df3 = df3[~np.isnan(df3['displacement_dx1 [mm]'])]
#     df3  = df3.groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)

#     depo = '{:s}_{:s}'.format(depo1,depo3)
#     vsensor = '{:s}_{:s}'.format(vsensor1,vsensor3)
#     df = pd.concat([df1, df2], axis=1, join='inner')
#     #df['hydrological_year'] = (df.index + pd.DateOffset(months=3)).year

#     #fig, ax1 = plt.subplots(1,1, figsize=(25/2.54, 10/2.54), facecolor='w', edgecolor='k')
#     #plt.subplots_adjust(left=0.07, right=0.93, bottom=0.08, top=0.99) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
#     fig, ax1 = plt.subplots(2,1, figsize=(12/2.54, 8/2.54), facecolor='w', edgecolor='k')
#     plt.subplots_adjust(left=0.14, right=0.84, bottom=0.09, top=0.99) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    
#     ax1[0].plot(df1['3d [mm]'], color=color2)
#     # ax1[0].plot(df1['e [mm]'], color=color2)

#     # ax1[0].set_ylabel('Inclination (°)', color=color2)
#     # ax1[0].set_ylim([-2,2])
#     # ax1[0].set_yticks(np.arange(-2,2,0.4))
#     for tl in ax1[0].get_yticklabels():
#         tl.set_color(color2)
#     ax1[0].tick_params(axis='y', colors=color2)
#     ax1[0].spines['left'].set_color(color2)

#     ax1[0].set_ylabel('Relative 3D coordinates (mm)', color=color2)
#     ax1[0].set_ylim([-25,250])
#     ax1[0].set_yticks(range(0,250,50))
    
#     ax1[0].xaxis.set_major_locator(years)
#     date_format = mdates.DateFormatter('%Y')
#     ax1[0].xaxis.set_major_formatter(date_format)
#     ax1[0].set_xlim(df1['3d [mm]'].index[0], df1['3d [mm]'].index[-1])

#     # t1, t2 = dataavailability_flags(df1['position []'])
#     # nodata_rectangles(ax1[0], t1, t2)
   
#     ax1[1].plot(df3['displacement_dx1 [mm]'])
#     ax1[1].set_xlim(df1['3d [mm]'].index[0], df1['3d [mm]'].index[-1])
        
#     fname = 'fig_{:s}_{:s}.png'.format(vsensor, depo)
#     plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,
    
# #%% MH33: gps (displacement and inclinometer)
# if fig_gps | ALL_FIG:
#     depo1 = 'MH33'
#     vsensor1 = 'gps_differential__rtklib__daily'
#     df1 = load_data(PATH_DATA + 'gnss_derived_data_products/', depo1, vsensor1, year=year_list, file_type='csv')
#     df1  = df1.groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)
#     df1['3d [mm]'] = np.sqrt((df1['n [m]']-df1['n [m]'][0])**2 + (df1['e [m]']-df1['e [m]'][0])**2 + (df1['h [m]']-df1['h [m]'][0])**2) * 1000
#     df1['e [mm]'] = np.sqrt((df1['e [m]']-df1['e [m]'][0])**2) * 1000

#     depo2 = 'MH33'
#     vsensor2 = 'gps_inclinometer'
#     df2 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo2, vsensor2, year=year_list, file_type='csv')
#     df2  = df2.groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)
    
#     depo3 = 'MH06'
#     vsensor3 = 'displacement'
#     df3 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo3, vsensor3, year=year_list, file_type='csv')
#     df3 = df3[~np.isnan(df3['displacement_dx2 [mm]'])]
#     df3  = df3.groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)

#     depo = '{:s}_{:s}'.format(depo1,depo3)
#     vsensor = '{:s}_{:s}'.format(vsensor1,vsensor3)
#     df = pd.concat([df1, df2], axis=1, join='inner')
#     #df['hydrological_year'] = (df.index + pd.DateOffset(months=3)).year

#     #fig, ax1 = plt.subplots(1,1, figsize=(25/2.54, 10/2.54), facecolor='w', edgecolor='k')
#     #plt.subplots_adjust(left=0.07, right=0.93, bottom=0.08, top=0.99) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
#     fig, ax1 = plt.subplots(2,1, figsize=(12/2.54, 8/2.54), facecolor='w', edgecolor='k')
#     plt.subplots_adjust(left=0.14, right=0.84, bottom=0.09, top=0.99) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    
#     ax1[0].plot(df1['3d [mm]'], color=color2)
#     # ax1[0].plot(df1['e [mm]'], color=color2)

#     # ax1[0].set_ylabel('Inclination (°)', color=color2)
#     # ax1[0].set_ylim([-2,2])
#     # ax1[0].set_yticks(np.arange(-2,2,0.4))
#     for tl in ax1[0].get_yticklabels():
#         tl.set_color(color2)
#     ax1[0].tick_params(axis='y', colors=color2)
#     ax1[0].spines['left'].set_color(color2)

#     ax1[0].set_ylabel('Relative 3D coordinates (mm)', color=color2)
#     ax1[0].set_ylim([-25,125])
#     ax1[0].set_yticks(range(0,100,10))
    
#     ax1[0].xaxis.set_major_locator(years)
#     date_format = mdates.DateFormatter('%Y')
#     ax1[0].xaxis.set_major_formatter(date_format)
#     ax1[0].set_xlim(df1['3d [mm]'].index[0], df1['3d [mm]'].index[-1])

#     t1, t2 = dataavailability_flags(df1['position []'])
#     nodata_rectangles(ax1[0], t1, t2)
   
#     ax2 = ax1[0].twinx()
#     ax2.plot(df2['inclinometer_west [°]'] - df2['inclinometer_west [°]'][df2['inclinometer_west [°]'].notna()][0], color=color1, zorder=2) #, linewidth=1 #ax3.set_ylim([44,50])
#     # ax2.plot(df2['inclinometer_north [°]'] - df2['inclinometer_north [°]'][df2['inclinometer_north [°]'].notna()][0], color=color1, zorder=2) #, linewidth=1 #ax3.set_ylim([44,50])
        
#     ax2.set_ylabel('Inclination North (°)', color=color1)
#     ax2.set_ylim([-0.25,2.25])
#     ax2.set_yticks(np.arange(0,2.5,0.5))
#     for tl in ax2.get_yticklabels():
#         tl.set_color(color1)
#     ax2.tick_params(axis='y', colors=color1)
#     ax2.spines['right'].set_color(color1)
            
#     ax1[1].plot(df3['displacement_dx2 [mm]'])
#     ax1[1].set_xlim(df1['3d [mm]'].index[0], df1['3d [mm]'].index[-1])
        
#     fname = 'fig_{:s}_{:s}.png'.format(vsensor, depo)
#     plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,
    

# #%% MH35: gps (displacement and inclinometer)
# if fig_gps | ALL_FIG:
#     depo1 = 'MH35'
#     vsensor1 = 'gps_differential__rtklib__daily'
#     df1 = load_data(PATH_DATA + 'gnss_derived_data_products/', depo1, vsensor1, year=year_list, file_type='csv')
#     df1  = df1.groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)
#     df1['3d [mm]'] = np.sqrt((df1['n [m]']-df1['n [m]'][0])**2 + (df1['e [m]']-df1['e [m]'][0])**2 + (df1['h [m]']-df1['h [m]'][0])**2) * 1000
#     df1['horiz [mm]'] = np.sqrt((df1['n [m]']-df1['n [m]'][0])**2 + (df1['e [m]']-df1['e [m]'][0])**2) * 1000

#     depo2 = 'MH35'
#     vsensor2 = 'gps_inclinometer'
#     df2 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo2, vsensor2, year=year_list, file_type='csv')
#     df2  = df2.groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)
    
#     depo3 = 'MH09'
#     vsensor3 = 'displacement'
#     df3 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo3, vsensor3, year=year_list, file_type='csv')
#     df3 = df3[~np.isnan(df3['displacement_dx3 [mm]'])]
#     df3  = df3.groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)

#     depo = '{:s}_{:s}'.format(depo1,depo3)
#     vsensor = '{:s}_{:s}'.format(vsensor1,vsensor3)
#     df = pd.concat([df1, df2], axis=1, join='inner')
#     #df['hydrological_year'] = (df.index + pd.DateOffset(months=3)).year
    
    
#     #fig, ax1 = plt.subplots(1,1, figsize=(25/2.54, 10/2.54), facecolor='w', edgecolor='k')
#     #plt.subplots_adjust(left=0.07, right=0.93, bottom=0.08, top=0.99) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
#     fig, ax1 = plt.subplots(2,1, figsize=(12/2.54, 8/2.54), facecolor='w', edgecolor='k')
#     plt.subplots_adjust(left=0.14, right=0.84, bottom=0.09, top=0.99) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    
#     # ax1[0].plot(df1['3d [mm]'], color=color2)
#     ax1[0].plot(df1['horiz [mm]'], color=color2)

#     # ax1.set_ylabel('Inclination (°)', color=color2)
#     # ax1.set_ylim([-2,2])
#     # ax1.set_yticks(np.arange(-2,2,0.4))
#     for tl in ax1[0].get_yticklabels():
#         tl.set_color(color2)
#     ax1[0].tick_params(axis='y', colors=color2)
#     ax1[0].spines['left'].set_color(color2)

#     ax1[0].set_ylabel('Displacement (mm)', color=color2)
#     ax1[0].set_ylim([0,25])
#     ax1[0].set_yticks(range(0,25,5))
    
#     ax1[0].xaxis.set_major_locator(years)
#     date_format = mdates.DateFormatter('%Y')
#     ax1[0].xaxis.set_major_formatter(date_format)
#     ax1[0].set_xlim(df1['3d [mm]'].index[0], df1['3d [mm]'].index[-1])

#     t1, t2 = dataavailability_flags(df1['position []'])
#     nodata_rectangles(ax1[0], t1, t2)
   
#     ax2 = ax1[0].twinx()
#     # ax2.plot(df2['inclinometer_west [°]'] - df2['inclinometer_west [°]'][df2['inclinometer_west [°]'].notna()][0], color=color1, zorder=2) #, linewidth=1 #ax3.set_ylim([44,50])
#     ax2.plot(df2['inclinometer_north [°]'] - df2['inclinometer_north [°]'][df2['inclinometer_north [°]'].notna()][0], color=color1, zorder=2) #, linewidth=1 #ax3.set_ylim([44,50])
    
#     ax2.set_ylabel('Inclination North (°)', color=color1)
#     ax2.set_ylim([-0.25,0.5])
#     ax2.set_yticks(np.arange(-0.25,0.5,0.25))
#     for tl in ax2.get_yticklabels():
#         tl.set_color(color1)
#     ax2.tick_params(axis='y', colors=color1)
#     ax2.spines['right'].set_color(color1)
        
#     ax1[1].plot(df3['displacement_dx3 [mm]'])
#     ax1[1].set_xlim(df1['3d [mm]'].index[0], df1['3d [mm]'].index[-1])
#     ax1[1].set_ylabel('Displacement (mm)')

#     fname = 'fig_{:s}_{:s}.png'.format(vsensor, depo)
#     plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,

    
    
#%%
if fig_dataavailability | ALL_FIG:
    DEPO_VS = {
            'MH01': {'displacement', 'temperature_fracture', 'temperature_rock'},  #ok
            'MH02': {'displacement', 'temperature_fracture', 'temperature_rock'},  #ok
            'MH03': {'displacement', 'temperature_fracture', 'temperature_rock'},  #ok
            'MH04': {'displacement', 'temperature_fracture', 'temperature_rock'},  #ok
            'MH05': {'temperature_fracture', 'resistivity_fracture'},              #ok but resistivity
            'MH06': {'displacement', 'temperature_rock'},                          #ok
            'MH07': {'temperature_fracture', 'resistivity_fracture'},              #ok but resistivity
            'MH08': {'displacement', 'temperature_rock'},                          #ok
            'MH09': {'displacement', 'temperature_fracture'},                      #ok
            'MH10': {'temperature_rock', 'resistivity_rock'},                      #ok but resistivity
            'MH11': {'temperature_rock', 'resistivity_rock'},                      #ok but resistivity
            'MH12': {'temperature_rock', 'resistivity_rock'},                      #ok but resistivity
            'MH18': {'displacement'},                                              #ok
            'MH20': {'displacement'},                                              #ok
            'MH21': {'displacement'},                                              #ok
            'MH22': {'displacement'},                                              #ok
            'MH27': {'temperature_rock'},                                          #ok
            'MH30': {'temperature_rock'},                                          #ok
            'MH46': {'temperature_rock'},                                          #ok
            'MH47': {'temperature_rock'},                                          #ok
    
            # 'MH13': {'waterpressure'},                                            #?
            'MH15': {'radiometer__conv'},                                          #ok
            'MH19': {'binary__mapped'},
            'MH25': {'vaisalawxt520windpth','vaisalawxt520prec'},                  #ok
            'MH51': {'vaisalawxt520windpth','vaisalawxt520prec'},                  #ok
    
            'MH33': {'gps_differential__batch__daily', 'gps_differential__rtklib__daily', 'gps_inclinometer'},        #ok
            'MH34': {'gps_differential__batch__daily', 'gps_differential__rtklib__daily', 'gps_inclinometer'},        #ok
            'MH35': {'gps_differential__batch__daily', 'gps_differential__rtklib__daily', 'gps_inclinometer'},        #ok
            'MH40': {'gps_differential__batch__daily', 'gps_differential__rtklib__daily'},                            #ok
            'MH42': {'gps_differential__batch__daily', 'gps_differential__rtklib__daily'},                            #ok
            'HOGR': {'gps_differential__batch__daily', 'gps_differential__rtklib__daily'},                            #ok
            }
    
    DEPO_VS_SORT = sorted(DEPO_VS.items(), key=lambda x: x[0])
    
    
    no_subplots = 0
    for depo in DEPO_VS_SORT:
        for vsensor in list(depo[1]):
            no_subplots += 1
    
    # print(no_subplots)

    fig, ax = plt.subplots(no_subplots,1, figsize=(25/2.54, 28/2.54), facecolor='w', edgecolor='k', sharex=True, sharey=True)
    plt.subplots_adjust(left=0.26, right=0.97, bottom=0.03, top=0.99, hspace=0) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
        
    xpos_ylabel = -0.36
        
    i=0
    for depo_vs in DEPO_VS_SORT:
    #    print(depo_vs)
        depo = depo_vs[0]
        if depo[0:2] == 'MH':
            deployment = 'matterhorn'
        position = depo[2:4]
        
        # Loop through values for a given key of dictionary DEPO_VS
        for vsensor in list(DEPO_VS[depo]):
            
            if not vsensor.startswith('binary__mapped'):
                df = pd.DataFrame()
                if df.empty:
                    try:
                        df = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
                    except:
                        pass
                if df.empty:
                    try:
                        df = load_data(PATH_DATA + 'gnss_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
                    except:
                        pass
                try:
                    t1, t2 = dataavailability_flags(df['position []'], daily=True)
                except:
                    t1 = [pd.DataFrame([1], index=[dt.datetime(1970,1,1,1)]).index[0]]
                    t2 = [pd.DataFrame([1], index=[dt.datetime(2050,1,1,1)]).index[0]]
                    pass

            else:
                # Check for binary images
                t1, t2 = testbinaryfiles(daily=True)
                pass

            # print(ax)
            # print(i)
            ax[i].set_facecolor('darkgreen')
            nodata_rectangles(ax[i], t1, t2, facecolor='white', alpha=1)
            
            if vsensor.startswith('gps_differential'):
                ax[i].set_ylabel('{:s} {:s}__{:s}'.format(depo, vsensor.split('__')[0], vsensor.split('__')[1]), rotation=0, ha='left')
            else:
                ax[i].set_ylabel('{:s} {:s}'.format(depo, vsensor.split('__')[0]), rotation=0, ha='left')
            
            ax[i].yaxis.set_label_coords(xpos_ylabel, 0.1)
            ax[i].grid()
            i += 1

    ax[0].set_xlim([dt.datetime(2008,9,1), dt.datetime(dt.datetime.today().year+1,1,1)])
    # ax[0].set_xlim([dt.datetime(2008,9,1), dt.datetime(dt.datetime.today().year+1,1,1)])
    ax[0].set_yticks([])
    
    fname = 'fig_data_availability.png'
    plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,






#%%
# if fig_trial | ALL_FIG:
#     #%% MH33: 
#     depo = 'MH33'
#     vsensor = 'gps_inclinometer'
#     df = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
    
#     fig, ax = plt.subplots(1,1, figsize=(13/2.54, 13/2.54), facecolor='w', edgecolor='k')
#     ax.plot(df['inclinometer_x [°]'],'.')
#     ax.plot(df['inclinometer_y [°]'],'.')
#     fig, ax = plt.subplots(1,1, figsize=(13/2.54, 13/2.54), facecolor='w', edgecolor='k')
#     ax.plot(df['inclinometer_x [°]'], df['inclinometer_y [°]'],'.')
#     ax.set_xlabel('inclinometer_x (°)')
#     ax.set_ylabel('inclinometer_y  (°)')
#     ax.set_xlim([-16,4])
#     ax.set_xticks(range(-16,8,4))
    
#     fname = 'fig_{:s}_{:s}.pdf'.format(vsensor, depo)
#     plt.savefig(PATH_FIGURES + fname , bbox_inches="tight", pad_inches=0, format='pdf')
    
#     #%% HOGR: 
#     depo = 'HOGR'
#     vsensor = 'gps_differential__rtklib__daily'
#     df = load_data(PATH_DATA + 'gnss_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
    
#     fig, ax = plt.subplots(1,1, figsize=(13/2.54, 13/2.54), facecolor='w', edgecolor='k')
#     ax.plot(df['e [m]'], df['n [m]'],'.')
#     #ax.set_xlabel('Temperature (°C)')
#     #ax.set_ylabel(r'Resistivity  (M$\Omega)$')
#     #ax.set_xlim([-16,4])
#     #ax.set_xticks(range(-16,8,4))
    
#     #fname = 'fig_{:s}_{:s}.pdf'.format(vsensor, depo)
#     #plt.savefig(PATH_FIGURES + fname , bbox_inches="tight", pad_inches=0, format='pdf')
    
#     #%% HOGR: 
#     depo = 'HOGR'
#     vsensor1 = 'gps_differential__rtklib__daily'
#     df = load_data(PATH_DATA + 'gnss_derived_data_products/', depo, vsensor1, year=year_list, file_type='csv')
#     df['3d [m]'] = np.sqrt((df['n [m]']-df['n [m]'][0])**2 + (df['e [m]']-df['e [m]'][0])**2 + (df['h [m]']-df['h [m]'][0])**2)
#     df  = df .groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)
    
#     depo = 'MH10'
#     vsensor2 = 'temperature_rock'
#     df1 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor2, year=year_list, file_type='csv')
#     df1  = df1 .groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)
    
#     vsensor = '{:s}_{:s}'.format(vsensor1,vsensor2)
#     df2 = pd.concat([df, df1], axis=1, join='inner')
#     df2['hydrological_year'] = (df.index + pd.DateOffset(months=3)).year
    
#     #fig, ax = plt.subplots(1,1, figsize=(26/2.54, 13/2.54), facecolor='w', edgecolor='k')
#     #ax.plot(df['3d [m]'])
    
#     fig, ax = plt.subplots(1,1, figsize=(7/2.54, 7/2.54), facecolor='w', edgecolor='k')
#     #fig, ax = plt.subplots(1,1, figsize=(13/2.54, 13/2.54), facecolor='w', edgecolor='k')
#     for y in np.unique(df2['hydrological_year']):
#         df_y = df2[df2['hydrological_year'] == y]
#         ax.plot(df_y['temperature_85cm [°C]'], df_y['3d [m]']*1000,'.')
#     ax.set_xlabel('Temperature rock [°C]')
#     ax.set_ylabel('Relative 3D coordinates [mm]')
#     ax.set_xlim([-12,12])
#     ax.set_xticks(range(-10,15,5))
#     ax.set_ylim([-10,250])
#     ax.set_yticks(range(0,250,50))
    
#     fname = 'fig_{:s}_{:s}.pdf'.format(vsensor, depo)
#     plt.savefig(PATH_FIGURES + fname , bbox_inches="tight", pad_inches=0, format='pdf')
    
    