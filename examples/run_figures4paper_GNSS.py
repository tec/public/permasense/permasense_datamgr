"""
Script to create the figures for the paper 
"In-situ observations of the Swiss periglacial environment
    using GNSS instruments" in the journal Earth System Science Data.

Copyright (c) 2022, WSL Institute for Snow and Avalanche Research SLF
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@date: June 23, 2022
@author: Samuel Weber, Jan Beutel
"""
# Import Libraries
import sys, inspect
import socket
import math
import os as os
import numpy as np
import pandas as pd
import datetime as dt
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from statsmodels.nonparametric.smoothers_lowess import lowess

# Remove matplotlib warning 
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
mpl.rc('figure', max_open_warning = 0)

# Load functions in relative path
pwd = os.path.dirname(os.path.abspath(__file__))
print(pwd)


current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir) 


from permasense.GSNdata import save_data, load_data, filter_data, clean_data
from permasense.plotting import dataavailability_flags, nodata_rectangles

os.chdir(pwd)

# For proper plotting on server
if socket.gethostname() == 'tik43x':
    mpl.use('Agg')

# Define which figures you want to create
ALL_FIG = False


fig04_BH13_overview = True
fig06_BH13_data_cleaning = True
fig07_kinematics_landforms = True

figA1_dataavailability = True
figA2_timeseries_position_reference = True
figA3_GNSS_LOWESS = True
figA4_GNSS_heatmap = True
figA5_GNSS_boxplot = True
figAx_timeseries_inclinometer = False
figB2_GG02_overview = True

# Define paths...
PATH_DATA = '../data/'
PATH_FIGURES = './figures_gnss/'
# ... and create folder PATH_DATA if it does not exist yet
os.makedirs(PATH_FIGURES, exist_ok=True)    

# Create different lists and data formatter
year_list = list(range(2010, dt.datetime.today().year + 1))
color_list = mpl.cm.tab10(np.linspace(0, 1, len(year_list))) # OR itertools.cycle(["r", "b", "g"]) with import itertools
if len(year_list) <= 10: 
    color_list = mpl.cm.tab10(range(0, len(year_list))) # OR itertools.cycle(["r", "b", "g"]) with import itertools
elif len(year_list) > 10:
    color_list = mpl.cm.tab20(range(0, len(year_list))) # OR itertools.cycle(["r", "b", "g"]) with import itertools

years = mdates.YearLocator()   # every year
biyearly =mdates.YearLocator(2)
months = mdates.MonthLocator()  # every month
yearsFmt = mdates.DateFormatter('%Y')


xpos_ylabel_square = -0.14

plt.rcParams.update({'font.size': 12})

color1 = 'black' #color = 'deepskyblue'
color2 = 'C2'#'deepskyblue' 
color3 = 'deepskyblue'

tbeg = dt.datetime(2011,1,1)
tend = dt.datetime(year_list[-1],1,1)
#downsampling_freq = '1Y'
#downsampling_freq = '1W' #weekly
downsampling_freq = '1440Min' #daily
# downsampling_freq = '10080Min'

#%% BH13 overview
if fig04_BH13_overview | ALL_FIG:
    plt.close('all')
    fig, ax = plt.subplots(5,1, figsize=(25/2.54, 30/2.54), facecolor='w', edgecolor='k', sharex=True)
    plt.subplots_adjust(left=0.08, right=0.93, bottom=0.02, top=0.99, hspace=0.1) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    
    #downsampling_freq = '1W' #weekly
    # downsampling_freq = '1440Min' #daily
    downsampling_freq_W = '10080Min' #weekly
    downsampling_freq_M = '1M'
    
    
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Panel 1: weather station data
    depo = 'DH13'
    vsensor1 = 'vaisalawxt520windpth'
    vsensor2 = 'vaisalawxt520prec'
    df1 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor1, year=year_list, file_type='csv')
    df1_mean = df1.groupby(pd.Grouper(freq=downsampling_freq_W)).aggregate(np.mean)
    df2 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor2, year=year_list, file_type='csv')
    df2 = df2.groupby(pd.Grouper(freq=downsampling_freq_M)).aggregate(np.sum)
    
    ax[0].plot(df1['temp_air [°C]'], color='C1')
    ax[0].axhline(y=0, linestyle='dotted', linewidth=1, color='black')
    ax2 = ax[0].twinx()
    ax2.bar(df2.index, df2['rain_accumulation [mm]'].to_list(), width=1, color='C0')
    
    ax[0].set_ylim([-30, 25])
    ax[0].set_ylabel('Air temperature [°C]')
    ax[0].yaxis.label.set_color('C1')
    ax[0].tick_params(axis='y', colors='C1')
    
    
    ax2.set_ylim([0, 300])
    ax2.set_ylabel('Precipitation [mm/month]')
    ax2.yaxis.label.set_color('C0')
    ax2.tick_params(axis='y', colors='C0')
    ax2.spines['right'].set_color('C0')
    ax2.spines['left'].set_color('C1')
    
    
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Panel 2: GNSS East component
    depo1 = 'BH13'
    depo2 = 'RD01'
    vsensor = 'gps_differential__rtklib__daily'
    df1 = load_data(PATH_DATA + 'gnss_derived_data_products/', depo1, vsensor, year=year_list, file_type='csv')
    df1_mean = df1.groupby(pd.Grouper(freq=downsampling_freq_W)).aggregate(np.mean)
    df2 = load_data(PATH_DATA + 'gnss_derived_data_products/', depo2, vsensor, year=year_list, file_type='csv')
    df2 = df2.groupby(pd.Grouper(freq=downsampling_freq_W)).aggregate(np.mean)
    
    lns1 = ax[1].plot((df1['e [m]']-df1['e [m]'][-1]), color='C3', label='Rock glacier station BH13')
    ax[1].axhline(y=0, linestyle='dotted', linewidth=1, color='black')
    ax2 = ax[1].twinx()
    lns2 = ax2.plot((df2['e [m]']-df2['e [m]'][-1]) * 1000, color='gray', label='Reference station RD01')
    
    lns = lns1 + lns2
    labs = [l.get_label() for l in lns]
    # ax[1].legend(lns, labs, loc=8, ncol=2, frameon=False)
    ax[1].legend(loc=3, ncol=1, frameon=False)
    ax2.legend(loc=4, ncol=1, frameon=False)
    
    ax[1].set_ylim([-10, 30])
    ax[1].set_ylabel('Displacement East [m]')
    ax[1].yaxis.label.set_color('C3')
    ax[1].tick_params(axis='y', colors='C3')
    ax[1].spines['left'].set_color('C3')
    
    ax2.set_ylim([-30, 10])
    ax2.set_ylabel('Displacement East [mm]')
    ax2.yaxis.label.set_color('gray')
    ax2.tick_params(axis='y', colors='gray')
    ax2.spines['right'].set_color('gray')
    ax2.spines['left'].set_color('C3')
    
    ax[1].set_zorder(1)
    ax[1].patch.set_visible(False)
    
    
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Panel 3: GNSS North component
    depo1 = 'BH13'
    depo2 = 'RD01'
    vsensor = 'gps_differential__rtklib__daily'
    df1 = load_data(PATH_DATA + 'gnss_derived_data_products/', depo1, vsensor, year=year_list, file_type='csv')
    df1_mean = df1.groupby(pd.Grouper(freq=downsampling_freq_W)).aggregate(np.mean)
    df2 = load_data(PATH_DATA + 'gnss_derived_data_products/', depo2, vsensor, year=year_list, file_type='csv')
    df2 = df2.groupby(pd.Grouper(freq=downsampling_freq_W)).aggregate(np.mean)
    
    lns1 = ax[2].plot((df1['n [m]']-df1['n [m]'][-1]), color='C3', label='Rock glacier station BH13')
    ax[2].axhline(y=0, linestyle='dotted', linewidth=1, color='black')
    ax2 = ax[2].twinx()
    lns2 = ax2.plot((df2['n [m]']-df2['n [m]'][-1]) * 1000, color='gray', label='Reference station RD01')
    
    lns = lns1 + lns2
    labs = [l.get_label() for l in lns]
    # ax[2].legend(lns, labs, loc=8, ncol=2, frameon=False)
    ax[2].legend(loc=3, ncol=1, frameon=False)
    ax2.legend(loc=4, ncol=1, frameon=False)
    
    ax[2].set_ylim([-30, 10])
    ax[2].set_ylabel('Displacement North [m]')
    ax[2].yaxis.label.set_color('C3')
    ax[2].tick_params(axis='y', colors='C3')
    ax[2].spines['left'].set_color('C3')
    
    ax2.set_ylim([-30, 10])
    ax2.set_ylabel('Displacement North [mm]')
    ax2.yaxis.label.set_color('gray')
    ax2.tick_params(axis='y', colors='gray')
    ax2.spines['right'].set_color('gray')
    ax2.spines['left'].set_color('C3')
    
    ax[2].set_zorder(1)
    ax[2].patch.set_visible(False)
    
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Panel 4 + 5: Inclination and Azimuth
    depo = 'BH13'
    vsensor = 'gps_inclinometer'
    df = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
    df  = df.groupby(pd.Grouper(freq=downsampling_freq_W)).aggregate(np.mean)
    df = df.join(pd.DataFrame( -np.arcsin( np.sqrt( np.sin(df['inclinometer_north [°]']*np.pi/180).pow(2) + np.sin(df['inclinometer_west [°]']*np.pi/180).pow(2) ) )*180/math.pi, columns=['inclination']))
    df = df.join(pd.DataFrame(180/math.pi * np.arctan2(-df['inclinometer_north [°]'], -df['inclinometer_west [°]']), columns=['azimuth']))
    df.loc[df['azimuth'] < 0, 'azimuth'] = df['azimuth'] + 360
       
    ax[3].plot(df['inclination'] - df['inclination'][df['inclination'].notna()][-1], color='C3', label='Rock glacier station BH13')
    ax[3].axhline(y=0, linestyle='dotted', linewidth=1, color='black')
    
    ax[4].plot(df['azimuth'] - df['azimuth'][df['azimuth'].notna()][-1], color='C3', label='Rock glacier station BH13')
    ax[4].axhline(y=0, linestyle='dotted', linewidth=1, color='black')
    
    ax[3].legend(loc=3, frameon=False)
    ax[3].set_ylabel('Inclination [°]')
    ax[3].set_ylim([-2, 6])
    ax[4].legend(loc=3, frameon=False)
    ax[4].set_ylabel('Azimuth [°]')
    ax[4].set_ylim([-120, 20])
    
    ax[3].yaxis.label.set_color('C3')
    ax[3].tick_params(axis='y', colors='C3')
    ax[3].spines['left'].set_color('C3')
    
    ax[4].yaxis.label.set_color('C3')
    ax[4].tick_params(axis='y', colors='C3')
    ax[4].spines['left'].set_color('C3')
    
    
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Optimize and save figure
    labelx1_pos = -0.07
    for i in range(5):
        ax[i].yaxis.set_label_coords(labelx1_pos, 0.5)
    
    ax[4].set_xlim([tbeg, tend])
    
    fname = 'fig04_BH13_overview.png'
    plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,




#%%
if fig06_BH13_data_cleaning | ALL_FIG:
    DEPO_VS = {
        'DH63': {'BH13': {'gps_inclinometer'}}, #090/000deg,         OK
        }
    
    AGGREGATION_INTERVAL = '60'
    AGGREGATE_GPS = False
    if np.core.defchararray.isdigit(AGGREGATION_INTERVAL):
        if int(AGGREGATION_INTERVAL) >= 1440:
            AGGREGATE_GPS = True
        AGGREGATION_INTERVAL = str(AGGREGATION_INTERVAL) + 'Min'
    else:
        if AGGREGATION_INTERVAL[-1] == "D" or AGGREGATION_INTERVAL[-1] == "W" or AGGREGATION_INTERVAL[-1] == "M":
            AGGREGATE_GPS = True
    
    # Loop through keys (depo) in dictionary DEPO_VS
    for depo, depo_line in DEPO_VS.items():
        for label in depo_line:
            print('Working on data of label {:s} and depo {:s}.'.format(label, depo))
    
        if depo[0:2] == 'JJ':
            deployment = 'jungfraujoch'
        elif depo[0:2] == 'AD':
            deployment = 'adm'
        elif depo[0:2] == 'MH':
            deployment = 'matterhorn'
        elif depo[0:2] == 'DH':
            deployment = 'dirruhorn'
        elif depo[0:2] == 'PE':
            deployment = 'permos'
        position = depo[2:4]
        
    # Step 1: Query data from GSN server and save it locally
                
    # Loop through values for a given key of dictionary DEPO_VS
        for vsensor in list(vs for vs in sorted(list(depo_line[label]))):
            df={}
            print(vsensor)
    
            # Step 2: Load data
            LOAD = 1
            if LOAD:
                print('Loading data', label, depo, deployment, vsensor)
                if vsensor == 'gps_differential__batch__daily':
                    df = load_data(PATH_DATA + '/gnss_data_raw/', label, vsensor, file_type='csv')
                    df_raw = df.copy()
                elif vsensor == 'gps_differential__rtklib__daily':
                    df = load_data(PATH_DATA + '/gnss_data_raw/', label, vsensor, file_type='csv')
                    df_raw = df.copy()
                else:
                    df = load_data(PATH_DATA + '/timeseries_data_raw/', label, vsensor, file_type='csv')
                    df_raw = df.copy()
    
            # Step 3 (optional): Filter data (according to reference values)
            FILTER = 1
            if FILTER:
                print('Filtering data', label, depo, deployment, vsensor)
                df = filter_data(df, depo, path_metadata='../')
            df_filt = df.copy()
            
            # Step 4 (optional): Clean data manually
            CLEAN = 1
            if CLEAN:
                print('Cleaning data', label, depo, deployment, vsensor)
                try:
                    df = clean_data(df, depo, vsensor, path_metadata='../')
                except Exception as i:
                    print('An issue with data cleaning raised - please check the data', i)
                    pass
            df_filt_clean = df.copy()
    
            # Step 5: Correct GPS data for moving reference HOGR
            if ((vsensor == 'gps_differential__batch__daily') or (vsensor == 'gps_differential__rtklib__daily')):
                if len(df) > 1:
                    t1 = df.index[0]
                    t2 = df.index[-1]
                    trng_reindex = pd.date_range(t1, t2, freq='24h')
    
                    label_list = ['MH33', 'MH34', 'MH35', 'MH40', 'MH43']
                    if label in label_list:
                        print("Correcting GPS for moving reference HOGR")
    
                        depo_HOGR = 'HOGR'
                        df_HOGR = load_data(PATH_DATA + '/gnss_data_raw/', depo_HOGR, vsensor, file_type='csv')
                        df_HOGR = df_HOGR.reindex(trng_reindex)
    
                        df['e'] = df['e'] + df_HOGR['e'] - df_HOGR['e'][0]
                        df['n'] = df['n'] + df_HOGR['n'] - df_HOGR['n'][0]
                        df['h'] = df['h'] + df_HOGR['h'] - df_HOGR['h'][0]
                    df_gps_corr = df.copy
                        
            # Step 6 (optional): Aggregate data
            # if AGGREGATION_INTERVAL > 0:
            print('Aggregating data', label, depo, deployment, vsensor)
            try:
                if vsensor == 'vaisalawxt520prec':
                    f = {'position':'mean', 'device_id':'mean', 'rain_accumulation':'sum', 'rain_duration':'sum', 'rain_intensity':'mean', 'rain_peak_intensity':'max', 'hail_accumulation':'sum', 'hail_duration':'sum', 'hail_intensity':'mean', 'hail_peak_intensity':'max'}
                    df['rain_accumulation'] = df['rain_accumulation'].diff()
    
                    df['rain_duration'] = df['rain_duration'].diff()
                    df['hail_accumulation'] = df['hail_accumulation'].diff()
                    df['hail_duration'] = df['hail_duration'].diff()
    
                    df[df < 0] = 0
    
                    df = df.groupby(pd.Grouper(freq=AGGREGATION_INTERVAL)).agg(f)
    
                elif vsensor == 'vaisalawxt520windpth':
                    f = {'position':'mean', 'device_id':'mean', 'wind_direction_minimum':'min', 'wind_direction_average':'mean', 'wind_direction_maximum':'max', 'wind_speed_minimum':'min', 'wind_speed_average':'mean', 'wind_speed_maximum':'max', 'temp_air':'mean', 'temp_internal':'mean', 'relative_humidity':'mean', 'air_pressure':'mean'}
                    df = df.groupby(pd.Grouper(freq=AGGREGATION_INTERVAL)).agg(f)
    
                elif vsensor == 'gps_differential__batch__daily':
                    if AGGREGATE_GPS:
                        df = df.groupby(pd.Grouper(freq=AGGREGATION_INTERVAL)).aggregate(np.mean)
                    else:
                        print('Copying GPS data, no aggregation.')
                        df = df
                elif vsensor == 'gps_differential__rtklib__daily':
                    if AGGREGATE_GPS:
                        print("inside gps")
                        df = df.groupby(pd.Grouper(freq=AGGREGATION_INTERVAL)).aggregate(np.mean)
                    else:
                        print('Copying GPS data, no aggregation.')
                        df = df
                else:
                    df = df.groupby(pd.Grouper(freq=AGGREGATION_INTERVAL)).aggregate(np.mean)
            except:
                print('There was an issue with time aggregation')
                pass
            df_agg = df.copy()
    
            # Step 7: Export to csv
            EXPORT2CSV = False
            if EXPORT2CSV:
                print('Exporting data derivatives', label, depo, deployment, vsensor)
                df_csv = df.copy()
                for col in ['device_id',  'ref1',  'ref2',  'ref3',  'ref4',  'ref5',  'ref6']:
                    try:
                        df_csv.drop([col], axis=1, inplace=True)
                    except:
                        pass
    
                try:
                    if vsensor == 'gps_differential__batch__daily':
                        save_data(df_csv, PATH_DATA + '/gnss_derived_data_products/', label, vsensor, deployment, yearly=True, iso=True, file_type='csv', float_format=4)
                    elif vsensor == 'gps_differential__rtklib__daily':
                        save_data(df_csv, PATH_DATA + '/gnss_derived_data_products/', label, vsensor, deployment, yearly=True, iso=True, file_type='csv', float_format=4)
                    else:
                        save_data(df_csv, PATH_DATA + '/timeseries_derived_data_products/', label, vsensor, deployment, yearly=True, iso=True, file_type='csv', float_format=4)
                except:
                    pass
                
            # Step 8: Plot for sanity check
            SANITY_PLOT = True
            if SANITY_PLOT:
                print('Plotting data', label, depo, deployment, vsensor)
                for i, col in enumerate(df_agg):
                    if col not in ['device_id', 'device_type', 'position', 'label', 'ref1', 'ref2', 'ref3', 'ref4', 'ref5', 'ref6', 'sd_e', 'sd_n', 'sd_h', 'version', 'reference_label', 'processing_time', 'ratio_of_fixed_ambiguities']:
                        plt.figure(figsize=(25/2.54, 15/2.54), facecolor='w', edgecolor='k')
                        plt.subplots_adjust(left=0.05, right=0.95, bottom=0.05, top=0.85) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
                        plt.axhline(y=0, linestyle='dotted', linewidth=1, color='black')
    
                        plt.plot(df_raw[col][~df_raw[col].isnull()], color='C4', label='raw', )
                        plt.plot(df_filt[col], color='C1', label='filtered')
                        plt.plot(df_filt_clean[col], color='C0', label='cleaned')
                        plt.plot(df_agg[col], color='C3', label='aggregated')
                        diff = df_agg[col].max() - df_agg[col].min()
                        try:
                            plt.ylim([df_agg[col].min() - diff/10, df_agg[col].max() + diff/10])
                        except:
                            pass
                        if label == 'BH13' and col == 'inclinometer_west':
                            plt.subplots_adjust(left=0.06, right=0.97, bottom=0.05, top=0.95)
                        else:
                            plt.title(' Label: {:s} \n Deployment: {:s} \n Position: {:s} \n VSENSOR: {:s}\n VARIABLE: {:s}'.format(label,deployment,depo[2:4],vsensor,col), loc='left')
                        if label == 'BH13' and col == 'inclinometer_west':
                            leg = plt.legend(loc=2, ncol=4, title="$\\bf{Rock\ glacier\ station\ BH13}$", frameon=False)                        
                            leg._legend_box.align = "left"
                            plt.ylabel('Inclination West [°]')
                        else:
                            plt.legend(loc="upper left")
                        
                        mpl.rcParams['agg.path.chunksize'] = 10000
                        plt.xlim([tbeg, tend])
                        if label == 'BH13' and col == 'inclinometer_west':
                            plt.savefig('{:s}/fig06_BH13_data_cleaning.png'.format(PATH_FIGURES), format='png', dpi=300)
                            plt.ylabel('Inclination [°]')
                        mpl.rcParams['agg.path.chunksize'] = 0
                
                print()
    
    
    
    
    
    







#%% Displacement rates for different landforms
if fig07_kinematics_landforms | ALL_FIG:
    
    plt.close('all')
    fig, ax = plt.subplots(4,1, figsize=(25/2.54, 30/2.54), facecolor='w', edgecolor='k', sharex=True)
    plt.subplots_adjust(left=0.08, right=0.98, bottom=0.02, top=0.99, hspace=0.1) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    
    #downsampling_freq = '1W' #weekly
    # downsampling_freq = '1440Min' #daily
    downsampling_freq_W = '10080Min' #weekly
    downsampling_freq_M = '1M'
    
    LEG_TITLES = ['Fast rock glaciers Mattertal', 
                  'Slow rock glaciers Mattertal',
                  'Landslides Mattertal',
                  'Steep rock-walls Matterhorn']
    
    DEPO_VS_LIST = [
        {
        'BH10': {'gps_differential__rtklib__daily'}, #280/270/294/312deg  OK
        'DI03': {'gps_differential__rtklib__daily','gps_inclinometer'}, #000deg,             OK
        'DI04': {'gps_differential__rtklib__daily','gps_inclinometer'}, #000deg,             OK
        'DI07': {'gps_differential__rtklib__daily','gps_inclinometer'}, #105/277/321/334deg, OK
        'GG02': {'gps_differential__rtklib__daily','gps_inclinometer'}, #205/041deg          OK
        },
        {
        'BH13': {'gps_differential__rtklib__daily','gps_inclinometer'}, #090/000deg,         OK
        'DI02': {'gps_differential__rtklib__daily','gps_inclinometer'}, #225/131/281/246deg, OK
        'GG01': {'gps_differential__rtklib__daily','gps_inclinometer'}, #325deg,             OK
        'RIT1': {'gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
        'ST02': {'gps_differential__rtklib__daily','gps_inclinometer'}, #004deg,             OK
        'ST05': {'gps_differential__rtklib__daily','gps_inclinometer'}, #124deg,             OK
        },
        {
        'BH03': {'gps_differential__rtklib__daily'},                    #                    OK
        'BH07': {'gps_differential__rtklib__daily','gps_inclinometer'}, #240deg,             OK
        'BH09': {'gps_differential__rtklib__daily','gps_inclinometer'}, #130deg,             OK
        'BH12': {'gps_differential__rtklib__daily','gps_inclinometer'}, #140deg,             OK
        'LS05': {'gps_differential__rtklib__daily','gps_inclinometer'}, #318/330deg,         OK
        'LS11': {'gps_differential__rtklib__daily','gps_inclinometer'}, #262deg,             OK
        'LS12': {'gps_differential__rtklib__daily','gps_inclinometer'}, #006deg,             OK
        'WYS1': {'gps_differential__rtklib__daily','gps_inclinometer'}, #170deg,             OK
        },
        {
        'MH33': {'gps_differential__rtklib__daily', 'gps_inclinometer'},        #ok
        'MH34': {'gps_differential__rtklib__daily', 'gps_inclinometer'},        #ok
        'MH35': {'gps_differential__rtklib__daily', 'gps_inclinometer'},        #ok
        'MH40': {'gps_differential__rtklib__daily'},                            #ok
        'HOGR': {'gps_differential__rtklib__daily'},                            #ok
        'MH43': {'gps_differential__rtklib__daily'},
        }
        ]
    
    
    
    for i, DEPO_VS in enumerate(DEPO_VS_LIST):
        print()
        print(i)
        # Loop through keys (depo) in dictionary DEPO_VS
        for depo, depo_line in DEPO_VS.items():
            print(depo)
            print(depo_line)
        
            for vsensor in list(vs for vs in sorted(list(depo_line))):
                if vsensor == 'gps_differential__rtklib__daily':
        
                    # Extract full months only to prevent artifacts
                    df = load_data(PATH_DATA + '/gnss_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
        
                    # Preparing Downsampled Data
                    df1d = df.resample('D').median()
                    df1d = df1d.interpolate(method='linear')
                    # df1 = df.rolling(15).median()
                    df1d['hydrological_year'] = (df1d.index + pd.DateOffset(months=3)).year
                    df1d['year'] = df1d.index.year
                    df1d['month'] = df1d.index.month
        
                    df1m  = df1d.groupby(pd.Grouper(freq='M')).diff()
                    df1m  = df1m.groupby(pd.Grouper(freq='M')).aggregate(np.sum)
                    df1m['hydrological_year'] = (df1m.index + pd.DateOffset(months=3)).year
                    df1m['hydrological_month'] = (df1m.index + pd.DateOffset(months=3)).month
                    df1m['year'] = df1m.index.year
                    df1m['month'] = df1m.index.month
                    
                    df1m_diff = pd.DataFrame((df1m['e [m]'].pow(2) + df1m['n [m]'].pow(2)).pow(1./2), columns=['horiz_displacement'])
                    df1m_diff = df1m_diff.join(pd.DataFrame((df1m['e [m]'].pow(2) + df1m['n [m]'].pow(2) + df1m['h [m]'].pow(2)).pow(1./2), columns=['total_displacement']))
        
                    ax[i].plot(df1m_diff['total_displacement'], drawstyle="steps-pre", label = depo) 
                    ax[i].set_ylabel('[m/month]')
                    leg = ax[i].legend(loc=2, ncol=2, frameon=False, title=LEG_TITLES[i])
                    leg._legend_box.align = 'left'
    
                        
                    # Manual labeling
                    if depo == 'LS05':
                        dt_list = df1m_diff[df1m_diff['total_displacement'] > 0.5]
                        for index, row in dt_list.iterrows():
                            print(row)
                            ax[i].annotate(str(round(row['total_displacement'], 1)), xy=(index - dt.timedelta(30),0.5), xytext=(index - dt.timedelta(90), 0.42), arrowprops=dict(arrowstyle="->", facecolor='black'), ha='right')
                    
                    if depo == 'MH43':
                        dt_list = df1m_diff[df1m_diff['total_displacement'] > 0.15]
                        for index, row in dt_list.iterrows():
                            print(row)
                            ax[i].annotate(str(round(row['total_displacement'], 2)), xy=(index - dt.timedelta(30),0.1), xytext=(index - dt.timedelta(90), 0.09), arrowprops=dict(arrowstyle="->", facecolor='black'), ha='right')
    
    
    
    
    
    
    
    
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Optimize and save figure
    labelx1_pos = -0.07
    for i in range(4):
        ax[i].yaxis.set_label_coords(labelx1_pos, 0.5)
    
    ax[0].set_xlim([tbeg, tend])
    
    ax[0].set_ylim([0,2])
    ax[1].set_ylim([0,0.8])
    ax[2].set_ylim([0,0.5])
    ax[3].set_ylim([0,0.1])
    
    fname = 'fig07_kinematics_landforms.png'
    plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,
    
    
    








#%% Data availability
if figA1_dataavailability | ALL_FIG:
    plt.close('all')
    DEPO_VS1 = {
            'Mattertal': {'fieldsite'},
            'BH03': {'gps_differential__rtklib__daily'},                    #                    OK
            'BH07': {'gps_differential__rtklib__daily','gps_inclinometer'}, #240deg,             OK
            'BH09': {'gps_differential__rtklib__daily','gps_inclinometer'}, #130deg,             OK
            'BH10': {'gps_differential__rtklib__daily','gps_inclinometer'}, #280/270/294/312deg  OK
            'BH12': {'gps_differential__rtklib__daily','gps_inclinometer'}, #140deg,             OK
            'BH13': {'gps_differential__rtklib__daily','gps_inclinometer'}, #090/000deg,         OK
            'DI02': {'gps_differential__rtklib__daily','gps_inclinometer'}, #225/131/281/246deg, OK
            'DI03': {'gps_differential__rtklib__daily','gps_inclinometer'}, #000deg,             OK
            'DI04': {'gps_differential__rtklib__daily','gps_inclinometer'}, #000deg,             OK
            'DI07': {'gps_differential__rtklib__daily','gps_inclinometer'}, #105/277/321/334deg, OK
            'DI55': {'gps_differential__rtklib__daily'},                    #                    OK
            'DI57': {'gps_differential__rtklib__daily'},                    #                    OK
            'DIS1': {'gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            'DIS2': {'gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            'GG01': {'gps_differential__rtklib__daily','gps_inclinometer'}, #325deg,             OK
            'GG02': {'gps_differential__rtklib__daily','gps_inclinometer'}, #205/041deg          OK
            'GG52': {'gps_differential__rtklib__daily'},                    #                    OK
            'GG66': {'gps_differential__rtklib__daily'}, #,                                      OK
            'GG67': {'gps_differential__rtklib__daily'}, #,                                      OK
            'GU02': {'gps_differential__rtklib__daily','gps_inclinometer'}, #335deg,             OK
            'GU03': {'gps_differential__rtklib__daily','gps_inclinometer'}, #010deg,             OK
            'GU04': {'gps_differential__rtklib__daily','gps_inclinometer'}, #014deg,             OK
            'LS01': {'gps_differential__rtklib__daily','gps_inclinometer'}, #110deg,             OK
            'LS04': {'gps_differential__rtklib__daily','gps_inclinometer'}, #225deg,             OK
            'LS05': {'gps_differential__rtklib__daily','gps_inclinometer'}, #318/330deg,         OK
            'LS06': {'gps_differential__rtklib__daily','gps_inclinometer'},                                  #000deg,             OK
            'LS11': {'gps_differential__rtklib__daily','gps_inclinometer'}, #262deg,             OK
            'LS12': {'gps_differential__rtklib__daily','gps_inclinometer'}, #006deg,             OK
            'RAND': {'gps_differential__rtklib__daily'},                    #,                   OK
            'RA01': {'gps_differential__rtklib__daily','gps_inclinometer'}, #000deg,             OK
            'RA02': {'gps_differential__rtklib__daily','gps_inclinometer'}, #000deg,             OK
            'RA03': {'gps_differential__rtklib__daily','gps_inclinometer'}, #000deg,             OK
            'RD01': {'gps_differential__rtklib__daily'},                    #                    OK
            'RG01': {'gps_differential__rtklib__daily'},                    #                    OK
            'RIT1': {'gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            'RL01': {'gps_differential__rtklib__daily','gps_inclinometer'}, #309deg              OK
            'SA01': {'gps_differential__rtklib__daily','gps_inclinometer'},                                  #000deg
            'SATT': {'gps_differential__rtklib__daily'},                                                     #
            'ST02': {'gps_differential__rtklib__daily','gps_inclinometer'}, #004deg,             OK
            'ST05': {'gps_differential__rtklib__daily','gps_inclinometer'}, #124deg,             OK 
            'WYS1': {'gps_differential__rtklib__daily','gps_inclinometer'}, #170deg,             OK
            # 'Mattertal': {'empty'},
            'DH13': {'vaisalawxt520prec'},                                           #
            'DH42': {'vaisalawxt520prec'},                                           #
            'DH68': {'vaisalawxt520prec'},                                           #
            'DH69': {'vaisalawxt520prec'},                                           #
            'DH73': {'radiometer__conv'},   
            'Matterhorn0': {'empty'},
            'Matterhorn': {'fieldsite'},
            'HOGR': {'gps_differential__rtklib__daily'},                            #ok
            'MH33': {'gps_differential__rtklib__daily', 'gps_inclinometer'},        #ok
            'MH34': {'gps_differential__rtklib__daily', 'gps_inclinometer'},        #ok
            'MH35': {'gps_differential__rtklib__daily', 'gps_inclinometer'},        #ok
            'MH40': {'gps_differential__rtklib__daily'},                            #ok
            'MH43': {'gps_differential__rtklib__daily'},
            # 'Matterhorn': {'empty'},
            'MH15': {'radiometer__conv'},                                                                            #ok
            'MH25': {'vaisalawxt520prec'},                  #ok
            'MH51': {'vaisalawxt520prec'},                  #ok
            'Saas Tal0': {'empty'},
            'Saas Tal': {'fieldsite'},
            'GRU1': {'gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            'JAE1': {'gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            'Val Blenio0': {'empty'},
            'Val Blenio': {'fieldsite'},
            'LAR1': {'gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            'LAR2': {'gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            'Engadine0': {'empty'},
            'Engadine': {'fieldsite'},
            'COR1': {'gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            'MUA1': {'gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            'SCH1': {'gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            }

    DEPO_VS2 = {          
            'HOGR': {'gps_differential__rtklib__daily'},                            #ok
            'MH33': {'gps_differential__rtklib__daily', 'gps_inclinometer'},        #ok
            'MH34': {'gps_differential__rtklib__daily', 'gps_inclinometer'},        #ok
            'MH35': {'gps_differential__rtklib__daily', 'gps_inclinometer'},        #ok
            'MH40': {'gps_differential__rtklib__daily'},                            #ok
            'MH43': {'gps_differential__rtklib__daily'},
            'MH15': {'radiometer__conv'},                                                                            #ok
            'MH25': {'vaisalawxt520prec'},                  #ok
            'MH51': {'vaisalawxt520prec'},                  #ok
            }
    DEPO_VS3 = {          
            'GRU1': {'gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            'JAE1': {'gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            }
    DEPO_VS4 = {          
            'LAR1': {'gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            'LAR2': {'gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            }
    DEPO_VS5 = {          
            'COR1': {'gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            'MUA1': {'gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            'SCH1': {'gps_differential__rtklib__daily','gps_inclinometer'}, #,                   OK
            }
    
    DEPO_VS = DEPO_VS1
    #DEPO_VS = sorted(DEPO_VS1.items(), key=lambda x: x[0])

    n=1
    while n < 2:
        if n == 2:
            DEPO_VS = DEPO_VS2
            #DEPO_VS = sorted(DEPO_VS2.items(), key=lambda x: x[0])
        # elif n == 3:
        #     DEPO_VS = DEPO_VS3
        #     #DEPO_VS = sorted(DEPO_VS2.items(), key=lambda x: x[0])

        no_subplots = 0

        for depo in DEPO_VS:
            no_subplots += 1
            # for vsensor in list(DEPO_VS[depo]):
            #     if not vsensor.startswith('gps_differential__batch'):
            #         no_subplots += 1
        
        print(no_subplots)
        fig, ax = plt.subplots(no_subplots,1, figsize=(25/2.54, 30/2.54), facecolor='w', edgecolor='k', sharex=True, sharey=True)
        plt.subplots_adjust(left=0.26, right=0.98, bottom=0.02, top=1.00, hspace=0) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2

          
        xpos_ylabel = -0.36


        ax[0].set_xlim([dt.datetime(2010,10,1), dt.datetime(dt.datetime.today().year,1,1)])
        ax[0].set_yticks([])
            
        i=0
        no_hlines = []
        
        for depo in DEPO_VS:

         
            # Loop through values for a given key of dictionary DEPO_VS
            for vsensor in sorted(list(DEPO_VS[depo])):
                # print(vsensor)
                if vsensor == 'gps_inclinometer' and 'gps_differential__rtklib__daily' in list(DEPO_VS[depo]):
                    continue
                # print(vsensor)
                # print(vsensor)
                df = pd.DataFrame()
                if df.empty:
                    try:
                        df = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
                    except:
                        pass
                if df.empty:
                    try:
                        df = load_data(PATH_DATA + 'gnss_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
                    except:
                        pass



                ax[i].set_facecolor('darkgreen')
                
                try:
                    t1, t2 = dataavailability_flags(df['position []'], daily=True)
                except:
                    t1 = [pd.DataFrame([1], index=[dt.datetime(1970,1,1,1)]).index[0]]
                    t2 = [pd.DataFrame([1], index=[dt.datetime(2050,1,1,1)]).index[0]]
                    pass
                    
 
                try:
                        nodata_rectangles(ax[i], t1, t2, facecolor='white', alpha=1)
                except:
                    pass
                
                if vsensor.startswith('gps_differential'):
                    if 'gps_inclinometer' in list(DEPO_VS[depo]):
                        ax[i].set_ylabel('{:s} (GNSS with inclination)'.format(depo ), rotation=0, ha='left')                    
                    else:
                        # ax[i].set_ylabel('{:s} {:s}__{:s}'.format(depo, vsensor.split('__')[0], vsensor.split('__')[1]), rotation=0, ha='left')
                        ax[i].set_ylabel('{:s} (GNSS)'.format(depo), rotation=0, ha='left')
                elif vsensor.startswith('vaisalawxt520'):
                    ax[i].set_ylabel('{:s} (weather data)'.format(depo), rotation=0, ha='left')
                elif vsensor.startswith('radiometer'):
                    ax[i].set_ylabel('{:s} (radiation)'.format(depo), rotation=0, ha='left')
                elif vsensor.startswith('fieldsite'):
                    ax[i].set_ylabel('{:s}'.format(depo), rotation=0, ha='left', fontweight='bold')
                    no_hlines.append(i)
                elif vsensor.startswith('empty'):
                    ax[i].set_ylabel(' ', rotation=0, ha='left')
                else:
                    ax[i].set_ylabel('{:s} {:s}'.format(depo, vsensor.split('__')[0]), rotation=0, ha='left')


                ax[i].yaxis.set_label_coords(xpos_ylabel, 0.1)
                # ax[i].grid()
                i += 1
            
        for ii in range(i):
            ax[ii].spines['left'].set_visible(False)
            ax[ii].spines['bottom'].set_visible(True)
            ax[ii].spines['right'].set_visible(False)
            ax[ii].spines['top'].set_visible(False)
        print(ii)
        for ii in no_hlines:
            if ii> 0:
                ax[ii-1].spines['bottom'].set_visible(False)
        
        fname = 'figA1_dataavailability' + '.png'
        plt.savefig(PATH_FIGURES + fname, format='png', dpi=300)

        n += 1




#%% RD01', 'RL01', 'RG01', 'RAND', 'HOGR': position GPS vs. time
if figA2_timeseries_position_reference | ALL_FIG:
    fig, ax = plt.subplots(3,1, figsize=(25/2.54, 25/2.54), facecolor='w', edgecolor='k', sharex=True)
    plt.subplots_adjust(left=0.09, right=0.98, bottom=0.05, top=0.99, hspace=0.1) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    
    downsampling_freq = '10080Min' #weekly

    depo_list = ['RD01', 'RL01', 'RG01', 'RAND', 'HOGR']
    label_list = ['RD01', 'RL01', 'RG01', 'RAND', 'HOGR']
    col_list = [color2, 'C3', 'C1', color3, 'C5']
    col_list = ['C1', 'C0', 'C3', 'C4', 'C5']
    # col_list = mpl.cm.Accent.colors
    counter=0
    for depo in depo_list:
        vsensor = 'gps_differential__rtklib__daily'
        #vsensor = 'gps_differential__batch__daily'
        df = load_data(PATH_DATA + 'gnss_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
        df = df.groupby(pd.Grouper(freq=downsampling_freq)).aggregate(np.mean)
        
        # plot without HOGR correction
        ax[0].plot((df['e [m]']-df['e [m]'][-1])*1000, color=col_list[counter], label=label_list[counter])
        ax[1].plot((df['n [m]']-df['n [m]'][-1])*1000, color=col_list[counter], label=label_list[counter])
        ax[2].plot((df['h [m]']-df['h [m]'][-1])*1000, color=col_list[counter], label=label_list[counter])

        counter += 1
    
    ax[0].set_ylabel('Relative coordinates East [mm]')
    ax[1].set_ylabel('Relative coordinates North [mm]')
    ax[2].set_ylabel('Relative coordinates Elevation [mm]')

    xpos_ylabel = -0.07
    for i in range(3):
        ax[i].yaxis.set_label_coords(xpos_ylabel, 0.5)
        ax[i].xaxis.set_minor_locator(years)
        ax[i].xaxis.set_major_locator(biyearly)
        ax[i].axhline(y=0, xmin=0, xmax=1, color = 'black', linestyle='dotted', linewidth=1)
        ax[i].legend(loc=1, ncol=5)
    
    date_format = mdates.DateFormatter('')
    ax[0].xaxis.set_major_formatter(date_format)
    date_format = mdates.DateFormatter('%Y')
    ax[2].xaxis.set_major_formatter(date_format)
    
    # Set same range but different limits (offset) for all panels
    if 1:
        #determine axes and their limits 
        ax_selec = [(axis, axis.get_ylim()) for axis in ax]
        
        #find maximum y-limit spread
        max_delta = max([lmax-lmin for _, (lmin, lmax) in ax_selec])
        
        #expand limits of all subplots according to maximum spread
        for axis, (lmin, lmax) in ax_selec:
            axis.set_ylim(lmin-(max_delta-(lmax-lmin))/2, lmax+(max_delta-(lmax-lmin))/2)
    
    # Set xlim
    if 0:
        #determine axes and their limits 
        ax_selec = [(axis, axis.get_xlim()) for axis in ax]
        
        #find minimum x-limit spread
        t1 = min([lmin for _, (lmin, lmax) in ax_selec])
        t2 = max([lmin for _, (lmin, lmax) in ax_selec])
    
    ax[2].set_xlim([tbeg, tend])
    
    fname = 'figA2_timeseries_position_reference.png'
    plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,







#%%

import seaborn as sns
plt.close('all')
DEPO_VS = {
    'DH63': {'BH13': {'gps_differential__rtklib__daily'}}, #090/000deg,         OK
    }


# Loop through keys (depo) in dictionary DEPO_VS
for depo, depo_line in DEPO_VS.items():
    for label in depo_line:
        print('Working on data of label {:s} and depo {:s}.'.format(label, depo))

    if depo[0:2] == 'MH':
        deployment = 'matterhorn'
    elif depo[0:2] == 'DH':
        deployment = 'dirruhorn'
    elif depo[0:2] == 'PE':
        deployment = 'permos'
    position = int(depo[2:4])

    
    linewidth_reg = 2

    for vsensor in list(vs for vs in sorted(list(depo_line[label]))):
        if vsensor == 'gps_differential__rtklib__daily':

### Extract full months only to prevent artifacts
            df = load_data(PATH_DATA + '/gnss_derived_data_products/', label, vsensor, year=year_list, file_type='csv')
            print(df.index[0])
            print(df.index[-1])
            t1 = df.index[0]
            t2 = df.index[-1]
            
            startday = df.index[0].day
            startday = t1 + pd.DateOffset(months=1) - pd.DateOffset(days=startday)
            endday = df.index[-1].day - 1
            endday = t2 - pd.DateOffset(days=endday)
            df = df[(df.index > startday) & (df.index < endday)]

            t1 = df.index[0]
            t2 = df.index[-1]
            trng_reindex = pd.date_range(t1, t2, freq='24h')


            print('Generating LOWESS smoothed data')
            # https://www.statsmodels.org/stable/generated/statsmodels.nonparametric.smoothers_lowess.lowess.html#statsmodels.nonparametric.smoothers_lowess.lowess
            
            rolling_window_median = 10
            
            fraction = 0.015    # The fraction of the data used when estimating each y-value.
            it_reweighting = 5  # The number of residual-based reweightings to perform.
            delta = 12.00        # Distance within which to use linear-interpolation instead of weighted regression.
        
            # tried 0.025 5
            lowess_e = lowess(df['e [m]'], df.index, frac = fraction, it = it_reweighting, delta = delta)
            lowess_n = lowess(df['n [m]'], df.index, frac = fraction, it = it_reweighting, delta = delta)
            lowess_h = lowess(df['h [m]'], df.index, frac = (1* fraction), it = (1* it_reweighting), delta = (1* delta))
            timestamp_e = pd.to_datetime((lowess_e[:,0]//1000000000).astype('datetime64[s]')).tz_localize('UTC')
            timestamp_n = pd.to_datetime((lowess_n[:,0]//1000000000).astype('datetime64[s]')).tz_localize('UTC')
            timestamp_h = pd.to_datetime((lowess_h[:,0]//1000000000).astype('datetime64[s]')).tz_localize('UTC')

            dfe = pd.DataFrame(lowess_e[:,1], index=timestamp_e, columns=['e_filt'])
            dfn = pd.DataFrame(lowess_n[:,1], index=timestamp_n, columns=['n_filt'])
            dfh = pd.DataFrame(lowess_h[:,1], index=timestamp_h, columns=['h_filt'])

            dfe = dfe.reindex(trng_reindex)
            dfn = dfn.reindex(trng_reindex)
            dfh = dfh.reindex(trng_reindex)

            df = df.join(dfe)
            df = df.join(dfn)
            df = df.join(dfh)


            # Preparing Downsampled Data
            df1d = df.resample('D').median()
            df1d = df1d.interpolate(method='linear')
            # df1 = df.rolling(15).median()
            df1d['hydrological_year'] = (df1d.index + pd.DateOffset(months=3)).year
            df1d['year'] = df1d.index.year
            df1d['month'] = df1d.index.month

            df1m  = df1d.groupby(pd.Grouper(freq='M')).diff()
            df1m  = df1m.groupby(pd.Grouper(freq='M')).aggregate(np.sum)
            df1m['hydrological_year'] = (df1m.index + pd.DateOffset(months=3)).year
            df1m['hydrological_month'] = (df1m.index + pd.DateOffset(months=3)).month
            df1m['year'] = df1m.index.year
            df1m['month'] = df1m.index.month

            df1a  = df1d.groupby(pd.Grouper(freq='Y')).diff()
            df1a  = df1a.groupby(pd.Grouper(freq='Y')).aggregate(np.sum)
            df1a['hydrological_year'] = (df1a.index + pd.DateOffset(months=3)).year
            df1a['year'] = df1a.index.year
            df1a['month'] = df1a.index.month

            df1ha  = df1d.groupby(pd.Grouper(key='hydrological_year')).diff()
            df1ha['hydrological_year'] = pd.to_datetime((df1ha.index + pd.DateOffset(months=3)).year, format='%Y') + pd.DateOffset(months=3) - pd.DateOffset(days=1)
            df1ha  = df1ha.groupby(pd.Grouper(key='hydrological_year')).aggregate(np.sum)
            df1ha['hydrological_year'] = (df1ha.index + pd.DateOffset(months=3)).year
            df1ha['year'] = df1ha.index.year
            df1ha['month'] = df1ha.index.month

            df1m_diff = pd.DataFrame((df1m['e [m]'].pow(2) + df1m['n [m]'].pow(2)).pow(1./2), columns=['horiz_displacement'])
            df1m_diff = df1m_diff.join(pd.DataFrame((df1m['e_filt'].pow(2) + df1m['n_filt'].pow(2)).pow(1./2), columns=['horiz_displacement_filt']))
        
            df1m_diff = df1m_diff.join(df1m['h [m]'])
            df1m_diff.rename(columns={'h [m]':'vertical_displacement'}, inplace=True)
            df1m_diff = df1m_diff.join(df1m['h_filt'])
            df1m_diff.rename(columns={'h_filt':'vertical_displacement_filt'}, inplace=True)
            
            df1m_diff = df1m_diff.join(pd.DataFrame((df1m['e [m]'].pow(2) + df1m['n [m]'].pow(2) + df1m['h [m]'].pow(2)).pow(1./2), columns=['total_displacement']))
            df1m_diff = df1m_diff.join(pd.DataFrame((df1m['e_filt'].pow(2) + df1m['n_filt'].pow(2) + df1m['h_filt'].pow(2)).pow(1./2), columns=['total_displacement_filt']))

            df1m_diff = df1m_diff.join(pd.DataFrame(180/math.pi * np.arctan2(df1m['e [m]'], df1m['n [m]']), columns=['horiz_azimuth']))
            df1m_diff.loc[df1m_diff['horiz_azimuth'] < 0, 'horiz_azimuth'] = df1m_diff['horiz_azimuth'] + 360
            df1m_diff = df1m_diff.join(pd.DataFrame(180/math.pi * np.arctan2(df1m['e_filt'], df1m['n_filt']), columns=['horiz_azimuth_filt']))
            df1m_diff.loc[df1m_diff['horiz_azimuth_filt'] < 0, 'horiz_azimuth_filt'] = df1m_diff['horiz_azimuth_filt'] + 360

            df1m_diff = df1m_diff.join(pd.DataFrame(90 - (180/math.pi * np.arccos(np.divide(df1m_diff['vertical_displacement'], df1m_diff['total_displacement']))), columns=['inclination']))
            df1m_diff = df1m_diff.join(pd.DataFrame(90 - (180/math.pi * np.arccos(np.divide(df1m_diff['vertical_displacement_filt'], df1m_diff['total_displacement_filt']))), columns=['inclination_filt']))

            df1m_diff['hydrological_year'] = (df1m_diff.index + pd.DateOffset(months=3)).year
            df1m_diff['hydrological_month'] = (df1m_diff.index + pd.DateOffset(months=3)).month
            df1m_diff['year'] = df1m_diff.index.year
            df1m_diff['month'] = df1m_diff.index.month

            df1a_diff = pd.DataFrame((df1a['e [m]'].pow(2) + df1a['n [m]'].pow(2) + df1a['h [m]'].pow(2)).pow(1./2), columns=['total_displacement'])
            df1a_diff = df1a_diff.join(pd.DataFrame((df1a['e_filt'].pow(2) + df1a['n_filt'].pow(2) + df1a['h_filt'].pow(2)).pow(1./2), columns=['total_displacement_filt']))
            df1a_diff['hydrological_year'] = (df1a_diff.index + pd.DateOffset(months=3)).year
            df1a_diff['year'] = df1a_diff.index.year
            df1a_diff['month'] = df1a_diff.index.month

            df1ha_diff = pd.DataFrame((df1ha['e [m]'].pow(2) + df1ha['n [m]'].pow(2) + df1ha['h [m]'].pow(2)).pow(1./2), columns=['total_displacement'])
            df1ha_diff = df1ha_diff.join(pd.DataFrame((df1ha['e_filt'].pow(2) + df1ha['n_filt'].pow(2) + df1ha['h_filt'].pow(2)).pow(1./2), columns=['total_displacement_filt']))
            df1ha_diff['hydrological_year'] = (df1ha_diff.index + pd.DateOffset(months=3)).year
            df1ha_diff['year'] = df1ha_diff.index.year
            df1ha_diff['month'] = df1ha_diff.index.month

            ### Now Plotting Stuff
            if figA3_GNSS_LOWESS | ALL_FIG:
                fig = plt.figure(facecolor='w', edgecolor='k', figsize=(50/2.54, 30/2.54))
                plt.subplots_adjust(left=0.05, right=0.97, bottom=0.04, top=0.95, wspace=0.3, hspace=0.7) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
                gs = fig.add_gridspec(6, 4)
                ax = {}
            
                ax[0] = fig.add_subplot(gs[0, 0])
                ax[1] = fig.add_subplot(gs[0, 1])
                ax[2] = fig.add_subplot(gs[0, 2])
                ax[3] = fig.add_subplot(gs[1, 0])
                ax[4] = fig.add_subplot(gs[1, 1])
                ax[5] = fig.add_subplot(gs[1, 2])
                ax[6] = fig.add_subplot(gs[2, :])
                ax[7] = fig.add_subplot(gs[3, :])
                ax[8] = fig.add_subplot(gs[4, :])
                ax[9] = fig.add_subplot(gs[5, :])
                ax[10]= fig.add_subplot(gs[0, 3])
                ax[11]= fig.add_subplot(gs[1, 3])
    
                l1 = ax[0].plot(df1d['e [m]'], 'b.', label = 'daily positions') 
                l2 = ax[0].plot(df1d['e [m]'].rolling(rolling_window_median).median(), color='gray', linewidth=linewidth_reg, label = 'rolling window median')
                l3 = ax[0].plot(df1d['e_filt'], color='red', linewidth=linewidth_reg, label ='LOWESS kernel')
                ax[0].set_title('Displacement East')
                ax[0].set_ylabel('[m]')
                # ax[0].legend()
    
                ax[1].plot(df1d['n [m]'], 'b.', label = 'daily positions') 
                ax[1].plot(df1d['n [m]'].rolling(rolling_window_median).median(), color='gray', linewidth=linewidth_reg, label = 'rolling window median')
                ax[1].plot(df1d['n_filt'], color='red', linewidth=linewidth_reg, label ='LOWESS kernel')
                ax[1].set_title('Displacement North')
                ax[1].set_ylabel('[m]')
                # ax[1].legend()
    
                ax[2].plot(df1d['h [m]'], 'b.', label = 'daily positions') 
                ax[2].plot(df1d['h [m]'].rolling(rolling_window_median).median(), color='gray', linewidth=linewidth_reg, label = 'rolling window median')
                ax[2].plot(df1d['h_filt'], color='red', linewidth=linewidth_reg, label ='LOWESS kernel')
                ax[2].set_ylabel('[m]')
                ax[2].set_title('Displacement altitude')
                # ax[2].legend()
    
                l4 = ax[3].plot(df1m['e [m]'], drawstyle="steps-mid", label = 'from daily positions') 
                l5 = ax[3].plot(df1m['e [m]'].rolling(rolling_window_median).median(), color='gray', linewidth=linewidth_reg, drawstyle="steps-mid", label = 'from rolling window median')
                l6 = ax[3].plot(df1m['e_filt'], color='red', linewidth=linewidth_reg, drawstyle="steps-mid", label ='LOWESS kernel')
                ax[3].set_ylabel('[m/month]')
                ax[3].set_title('Monthly displacement rate East')
                # ax[3].legend()
                
                ax[4].plot(df1m['n [m]'], drawstyle="steps-mid", label = 'from daily positions') 
                ax[4].plot(df1m['n [m]'].rolling(rolling_window_median).median(), color='gray', linewidth=linewidth_reg, drawstyle="steps-mid", label = 'from rolling window median')
                ax[4].plot(df1m['n_filt'], color='red', linewidth=linewidth_reg, drawstyle="steps-mid", label ='LOWESS kernel')
                ax[4].set_ylabel('[m/month]')
                ax[4].set_title('Monthly displacement rate North')
                # ax[4].legend()
    
                ax[5].plot(df1m['h [m]'], drawstyle="steps-mid", label = 'from daily positions') 
                ax[5].plot(df1m['h [m]'].rolling(rolling_window_median).median(), color='gray', linewidth=linewidth_reg, drawstyle="steps-mid", label = 'from rolling window median')
                ax[5].plot(df1m['h_filt'], color='red', linewidth=linewidth_reg, drawstyle="steps-mid", label ='LOWESS kernel')
                ax[5].set_ylabel('[m/month]')
                ax[5].set_title('Monthly displacement rate Altitude')
                # ax[5].legend()
    
                ax[6].plot(df1m_diff['horiz_displacement'], drawstyle="steps-pre", label = 'from daily positions') 
                ax[6].plot(df1m_diff['horiz_displacement_filt'], color='red', linewidth=linewidth_reg, drawstyle="steps-mid", label ='LOWESS kernel')
                ax[6].set_title('Monthly horizontal displacement rate')
                ax[6].set_ylabel('[m/month]')
                ax[6].legend(ncol=2)
                
                ax[7].plot(df1m_diff['total_displacement'], drawstyle="steps-pre", label = 'from daily positions') 
                ax[7].plot(df1m_diff['total_displacement_filt'], color='red', linewidth=linewidth_reg, drawstyle="steps-mid", label ='LOWESS kernel')
                ax[7].set_title('Monthly total displacement rate')
                ax[7].set_ylabel('[m/month]')
                ax[7].legend(ncol=2)
                
                ax[8].plot(df1a_diff['total_displacement'], drawstyle="steps-mid", label = 'from daily positions') 
                ax[8].plot(df1a_diff['total_displacement_filt'], color='red', linewidth=linewidth_reg, drawstyle="steps-mid", label ='LOWESS kernel')
                ax[8].set_title('Annual total displacement rate')
                ax[8].set_ylabel('[m/a]')
                ax[8].legend(ncol=2)
                
                ax[9].plot(df1ha_diff['total_displacement'], drawstyle="steps-mid", label = 'from daily positions') 
                ax[9].plot(df1ha_diff['total_displacement_filt'], color='red', linewidth=linewidth_reg, drawstyle="steps-mid", label ='LOWESS kernel')
                ax[9].set_title('Annual total displacement rate (hydrological year)')
                ax[9].set_ylabel('[m/a]')
                ax[9].legend(ncol=2)
                
                ax[10].axis('off')
                lns = l1 + l2 + l3
                labs = [l.get_label() for l in lns]
                ax[10].legend(lns, labs, loc=6, ncol=1, frameon=False)
                
                ax[11].axis('off')
                lns = l4 + l5 + l6
                labs = [l.get_label() for l in lns]
                ax[11].legend(lns, labs, loc=6, ncol=1, frameon=False)

                # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                # Optimize figure
                labelx1_pos = -0.16
                for i in range(6):
                    ax[i].yaxis.set_label_coords(labelx1_pos, 0.5)
                    for tick in ax[i].get_xticklabels():
                        tick.set_rotation(45)
                labelx1_pos = -0.03
                for i in [6,7,8,9]:
                    ax[i].yaxis.set_label_coords(labelx1_pos, 0.5)
                
                for i in range(10):
                    ax[i].set_xlim([t1, t2])
                
                    
                plt.savefig('{:s}figA3_GNSS_LOWESS_{:s}_{:s}.png'.format(PATH_FIGURES , label, vsensor), dpi=300)


            ### LEVELPLOT
            if figA4_GNSS_heatmap | ALL_FIG:
                print('Now Levelplotting')
    
                df1m_horiz = df1m_diff.pivot(values = 'horiz_displacement_filt', columns = 'hydrological_year', index = 'hydrological_month')
                df1m_vertical = df1m_diff.pivot(values = 'vertical_displacement_filt', columns = 'hydrological_year', index = 'hydrological_month')
                df1m_total = df1m_diff.pivot(values = 'total_displacement_filt', columns = 'hydrological_year', index = 'hydrological_month')
    
                fig, (l_ax1, l_ax2, l_ax3, ax) = plt.subplots(nrows=1, ncols=4, figsize = (40/2.54, 15/2.54))
                # fig = plt.figure(facecolor='w', edgecolor='k', figsize=(50/2.54, 30/2.54))
                plt.subplots_adjust(left=0.05, right=0.97, bottom=0.15, top=0.95, wspace=0.3, hspace=0.6) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
                linewidth = 0.5
                cmap = sns.color_palette('autumn', as_cmap = True)
                cmap_r = sns.color_palette('autumn_r', as_cmap = True)
                l_ax1 = plt.subplot(1, 3, 1)
                l_ax1 = sns.heatmap(df1m_horiz, linewidth = linewidth, cmap = cmap_r, yticklabels=['10', '11', '12', '1', '2', '3', '4', '5', '6', '7', '8', '9'])
                l_ax1.set_title('Horizontal displacement rate [m/month]')
                l_ax1.set_ylabel('Month')
                l_ax1.set_xlabel('Hydrological Year')          
    
                l_ax1 = plt.subplot(1, 3, 2)
                l_ax2 = sns.heatmap(df1m_vertical, linewidth = linewidth, cmap = cmap, yticklabels=['10', '11', '12', '1', '2', '3', '4', '5', '6', '7', '8', '9'])
                l_ax2.set_title('Vertical displacement rate [m/month]')
                l_ax2.set_ylabel('Month')
                l_ax2.set_xlabel('Hydrological Year')
                
                l_ax1 = plt.subplot(1, 3, 3)
                l_ax3 = sns.heatmap(df1m_total, linewidth = linewidth, cmap = cmap_r, yticklabels=['10', '11', '12', '1', '2', '3', '4', '5', '6', '7', '8', '9'])
                l_ax3.set_title('Total displacement rate [m/month]')
                l_ax3.set_ylabel('Month')
                l_ax3.set_xlabel('Hydrological Year')
    
                ## Inclination/Azimuth
                # sns.set_theme(style="whitegrid")
                # ax = plt.subplot(1, 4, 4)
                # sns.despine(fig, left=True, bottom=True)
                # sns.scatterplot(x="horiz_azimuth_filt", y="inclination_filt", data = df1m_diff, hue = 'hydrological_year', linewidth=0.5, ax=ax)
                # ax.set_xlim(0, 360)
                # ax.set_ylim(-90, 90)
                # ax.set_ylabel('Vertical Displacement Inclination')
                # ax.set_xlabel('Horizontal Displacement Azimuth')
                
                plt.savefig('{:s}figA4_GNSS_heatmap_{:s}_{:s}.png'.format(PATH_FIGURES , label, vsensor), dpi=300)


            #SEASONAL BOX PLOTS
            if figA5_GNSS_boxplot | ALL_FIG:
                fig, (f_ax1, f_ax2, f_ax3) = plt.subplots(1, 3, figsize=(40/2.54, 15/2.54))
                # fig = plt.figure(facecolor='w', edgecolor='k', figsize=(50/2.54, 30/2.54))
                plt.subplots_adjust(left=0.05, right=0.97, bottom=0.15, top=0.95, wspace=0.3, hspace=0.6) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2

                palette = sns.color_palette()
                f_ax1 = plt.subplot(1, 3, 1)
                sns.lineplot(df1m_diff['hydrological_month'], df1m_diff['total_displacement_filt'], hue=df1m_diff['hydrological_year'])
                f_ax1.set_title('Monthly displacement rate')
                f_ax1.set_xlabel('Month')
                f_ax1.set_ylabel('[m/month]')
                handles, labels = f_ax1.get_legend_handles_labels()
                f_ax1.legend(reversed(handles), reversed(labels), title='Year')
    
                f_ax2 = plt.subplot(1, 3, 2)
                sns.boxplot(df1m_diff['hydrological_month'], df1m_diff['total_displacement_filt'], ax=f_ax2)
                f_ax2.set_title('Monthly displacement rate')
                f_ax2.set_xlabel('Month')
                f_ax2.set_ylabel('[m/month]')
    
                f_ax3 = plt.subplot(1, 3, 3)
                sns.boxplot(df1m_diff['hydrological_year'], df1m_diff['total_displacement_filt'], ax=f_ax3)
                f_ax3.set_title('Monthly displacement rate')
                f_ax3.set_xlabel('Hydrological year')
                f_ax3.set_ylabel('[m/month]')
                plt.xticks(rotation = 45)
    
                plt.savefig('{:s}figA5_GNSS_boxplot_{:s}_{:s}.png'.format(PATH_FIGURES , label, vsensor), dpi=300)
                plt.show()
            


#%% MH33, MH34, MH35: inclinometer GPS vs. time
figAx_timeseries_inclinometer = True
if figAx_timeseries_inclinometer | ALL_FIG:

    fig, ax = plt.subplots(2,1, figsize=(25/2.54, 25/2.54), facecolor='w', edgecolor='k', sharex=True)
    plt.subplots_adjust(left=0.09, right=0.97, bottom=0.03, top=0.99, hspace=0.05) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    
    ### Now the inclinometers
    #downsampling_freq = '1W' #weekly
    # downsampling_freq = '1440Min' #daily
    downsampling_freq = '10080Min' #weekly

    # depo_list = ['MH33', 'MH34', 'MH35']
    # label_list = ['MH33', 'MH34', 'MH35']           
    depo_list = ['LS05']
    label_list = ['LS05']
    col_list = [color2, 'C3', 'C1', color3]
    counter=0
    for depo2 in depo_list:
        # depo2 = 'MH34'
        vsensor2 = 'gps_inclinometer'
        df = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo2, vsensor2, year=year_list, file_type='csv')
        df  = df.groupby(pd.Grouper(freq=downsampling_freq)).aggregate(np.mean)
        df = df.join(pd.DataFrame( -np.arcsin( np.sqrt( np.sin(df['inclinometer_north [°]']*np.pi/180).pow(2) + np.sin(df['inclinometer_west [°]']*np.pi/180).pow(2) ) )*180/math.pi, columns=['inclination']))
        df = df.join(pd.DataFrame(180/math.pi * np.arctan2(-df['inclinometer_north [°]'], -df['inclinometer_west [°]']), columns=['azimuth']))
        df.loc[df['azimuth'] < 0, 'azimuth'] = df['azimuth'] + 360
       
        # plot without HOGR correction
        ax[0].plot(df['inclination'] - df['inclination'][df['inclination'].notna()][-1], color=col_list[counter], label=label_list[counter])
        ax[1].plot(df['azimuth'] - df['azimuth'][df['azimuth'].notna()][-1], color=col_list[counter], label=label_list[counter])
        # ax[2].plot((df['h [m]']-df['h [m]'][-1])*1000, color=col_list[counter], label=label_list[counter])

        counter += 1
    
    ax[0].set_ylabel('Inclination [°]')
    ax[1].set_ylabel('Azimuth [°]')

    xpos_ylabel = -0.07
    for i in range(2):
        ax[i].yaxis.set_label_coords(xpos_ylabel, 0.5)
        ax[i].xaxis.set_minor_locator(years)
        ax[i].xaxis.set_major_locator(biyearly)
        ax[i].axhline(y=0, xmin=0, xmax=1, color = 'black', linestyle='dotted', linewidth=1)
        ax[i].legend(loc=1, ncol=3)
    
    date_format = mdates.DateFormatter('')
    ax[0].xaxis.set_major_formatter(date_format)
    date_format = mdates.DateFormatter('%Y')
    ax[1].xaxis.set_major_formatter(date_format)
    
    # ax[0].set_ylim([-450, 150])
    # ax[1].set_ylim([-50, 550])
    # ax[2].set_ylim([-150, 450])
    ax[1].set_xlim([tbeg, tend])
    
    fname = 'figAx_timeseries_inclinometer.png'
    plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,



#%% BH13 overview
if figB2_GG02_overview | ALL_FIG:
    plt.close('all')
    fig, ax = plt.subplots(4,1, figsize=(25/2.54, 24/2.54), facecolor='w', edgecolor='k', sharex=True)
    plt.subplots_adjust(left=0.08, right=0.93, bottom=0.03, top=0.99, hspace=0.1) # left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2
    
    #downsampling_freq = '1W' #weekly
    # downsampling_freq = '1440Min' #daily
    downsampling_freq_W = '10080Min' #weekly
    downsampling_freq_M = '1M'
    
    
    # # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # # Panel 1: weather station data
    # depo = 'DH13'
    # vsensor1 = 'vaisalawxt520windpth'
    # vsensor2 = 'vaisalawxt520prec'
    # df1 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor1, year=year_list, file_type='csv')
    # df1_mean = df1.groupby(pd.Grouper(freq=downsampling_freq_W)).aggregate(np.mean)
    # df2 = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor2, year=year_list, file_type='csv')
    # df2 = df2.groupby(pd.Grouper(freq=downsampling_freq_M)).aggregate(np.sum)
    
    # ax[0].plot(df1['temp_air [°C]'], color='C1')
    # ax[0].axhline(y=0, linestyle='dotted', linewidth=1, color='black')
    # ax2 = ax[0].twinx()
    # ax2.bar(df2.index, df2['rain_accumulation [mm]'].to_list(), width=1, color='C0')
    
    # ax[0].set_ylim([-30, 25])
    # ax[0].set_ylabel('Air temperature [°C]')
    # ax[0].yaxis.label.set_color('C1')
    # ax[0].tick_params(axis='y', colors='C1')
    
    
    # ax2.set_ylim([0, 300])
    # ax2.set_ylabel('Precipitation [mm/month]')
    # ax2.yaxis.label.set_color('C0')
    # ax2.tick_params(axis='y', colors='C0')
    # ax2.spines['right'].set_color('C0')
    # ax2.spines['left'].set_color('C1')
    
    
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Panel 2: GNSS East component
    depo1 = 'GG02'
    depo2 = 'RG01'
    vsensor = 'gps_differential__rtklib__daily'
    df1 = load_data(PATH_DATA + 'gnss_derived_data_products/', depo1, vsensor, year=year_list, file_type='csv')
    df1_mean = df1.groupby(pd.Grouper(freq=downsampling_freq_W)).aggregate(np.mean)
    df2 = load_data(PATH_DATA + 'gnss_derived_data_products/', depo2, vsensor, year=year_list, file_type='csv')
    df2 = df2.groupby(pd.Grouper(freq=downsampling_freq_W)).aggregate(np.mean)
    
    lns1 = ax[0].plot((df1['e [m]']-df1['e [m]'][-1]), color='C3', label='Rock glacier station GG02')
    ax[0].axhline(y=0, linestyle='dotted', linewidth=1, color='black')
    ax2 = ax[0].twinx()
    lns2 = ax2.plot((df2['e [m]']-df2['e [m]'][-1]) * 1000, color='gray', label='Reference station RG01')
    
    lns = lns1 + lns2
    labs = [l.get_label() for l in lns]
    # ax[0].legend(lns, labs, loc=8, ncol=2, frameon=False)
    ax[0].legend(loc=3, ncol=1, frameon=False)
    ax2.legend(loc=4, ncol=1, frameon=False)
    
    ax[0].set_ylim([-10, 15])
    ax[0].set_ylabel('Displacement East [m]')
    ax[0].yaxis.label.set_color('C3')
    ax[0].tick_params(axis='y', colors='C3')
    ax[0].spines['left'].set_color('C3')
    
    ax2.set_ylim([-15, 10])
    ax2.set_ylabel('Displacement East [mm]')
    ax2.yaxis.label.set_color('gray')
    ax2.tick_params(axis='y', colors='gray')
    ax2.spines['right'].set_color('gray')
    ax2.spines['left'].set_color('C3')
    
    ax[0].set_zorder(1)
    ax[0].patch.set_visible(False)
    
    
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Panel 3: GNSS North component
    depo1 = 'GG02'
    depo2 = 'RG01'
    vsensor = 'gps_differential__rtklib__daily'
    df1 = load_data(PATH_DATA + 'gnss_derived_data_products/', depo1, vsensor, year=year_list, file_type='csv')
    df1_mean = df1.groupby(pd.Grouper(freq=downsampling_freq_W)).aggregate(np.mean)
    df2 = load_data(PATH_DATA + 'gnss_derived_data_products/', depo2, vsensor, year=year_list, file_type='csv')
    df2 = df2.groupby(pd.Grouper(freq=downsampling_freq_W)).aggregate(np.mean)
    
    lns1 = ax[1].plot((df1['n [m]']-df1['n [m]'][-1]), color='C3', label='Rock glacier station GG02')
    ax[1].axhline(y=0, linestyle='dotted', linewidth=1, color='black')
    ax2 = ax[1].twinx()
    lns2 = ax2.plot((df2['n [m]']-df2['n [m]'][-1]) * 1000, color='gray', label='Reference station RG01')
    
    lns = lns1 + lns2
    labs = [l.get_label() for l in lns]
    # ax[1].legend(lns, labs, loc=8, ncol=2, frameon=False)
    ax[1].legend(loc=3, ncol=1, frameon=False)
    ax2.legend(loc=4, ncol=1, frameon=False)
    
    ax[1].set_ylim([-15, 10])
    ax[1].set_ylabel('Displacement North [m]')
    ax[1].yaxis.label.set_color('C3')
    ax[1].tick_params(axis='y', colors='C3')
    ax[1].spines['left'].set_color('C3')
    
    ax2.set_ylim([-15, 10])
    ax2.set_ylabel('Displacement North [mm]')
    ax2.yaxis.label.set_color('gray')
    ax2.tick_params(axis='y', colors='gray')
    ax2.spines['right'].set_color('gray')
    ax2.spines['left'].set_color('C3')
    
    ax[1].set_zorder(1)
    ax[1].patch.set_visible(False)
    
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Panel 4 + 5: Inclination and Azimuth
    depo = 'GG02'
    vsensor = 'gps_inclinometer'
    df = load_data(PATH_DATA + 'timeseries_derived_data_products/', depo, vsensor, year=year_list, file_type='csv')
    df  = df.groupby(pd.Grouper(freq=downsampling_freq_W)).aggregate(np.mean)
    df = df.join(pd.DataFrame( -np.arcsin( np.sqrt( np.sin(df['inclinometer_north [°]']*np.pi/180).pow(2) + np.sin(df['inclinometer_west [°]']*np.pi/180).pow(2) ) )*180/math.pi, columns=['inclination']))
    df = df.join(pd.DataFrame(180/math.pi * np.arctan2(-df['inclinometer_north [°]'], -df['inclinometer_west [°]']), columns=['azimuth']))
    df.loc[df['azimuth'] < 0, 'azimuth'] = df['azimuth'] + 360
       
    ax[2].plot(df['inclination'] - df['inclination'][df['inclination'].notna()][-1], color='C3', label='Rock glacier station GG02')
    ax[2].axhline(y=0, linestyle='dotted', linewidth=1, color='black')
    
    ax[3].plot(df['azimuth'] - df['azimuth'][df['azimuth'].notna()][-1], color='C3', label='Rock glacier station GG02')
    ax[3].axhline(y=0, linestyle='dotted', linewidth=1, color='black')
    
    ax[2].legend(loc=3, frameon=False)
    ax[2].set_ylabel('Inclination [°]')
    ax[2].set_ylim([-12, 8])
    ax[3].legend(loc=3, frameon=False)
    ax[3].set_ylabel('Azimuth [°]')
    ax[3].set_ylim([-290, 30])
    
    ax[2].yaxis.label.set_color('C3')
    ax[2].tick_params(axis='y', colors='C3')
    ax[2].spines['left'].set_color('C3')
    
    ax[3].yaxis.label.set_color('C3')
    ax[3].tick_params(axis='y', colors='C3')
    ax[3].spines['left'].set_color('C3')
    
    
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Optimize and save figure
    labelx1_pos = -0.07
    for i in range(4):
        ax[i].yaxis.set_label_coords(labelx1_pos, 0.5)
    
    ax[3].set_xlim([tbeg, tend])
    
    fname = 'figB2_GG02_overview.png'
    plt.savefig(PATH_FIGURES + fname, format='png', dpi=300) #bbox_inches="tight", pad_inches=0,



#%%
plt.close('all')