'''
Copyright (c) 2021, Swiss Federal Institute of Technology (ETH Zurich)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@date: March 31, 2021
@author: Samuel Weber, Jan Beutel and Matthias Meyer
'''

def testbinaryfiles(daily=False):
  import os as os
  import numpy as np
  import pandas as pd
  import datetime as dt
  from pathlib import Path

  imagepath = '../mh_data/timelapse_images/'
  starttime=dt.datetime(1970,1,1,0,0,0)
  endtime=dt.datetime(2050,1,1,0,0,0)
  # starttime=dt.datetime(2010,6,22,0,0,0)
  # endtime=dt.datetime(2021,3,13,0,0,0)

  # print()
  # print('starttime %s'% starttime)

  df2 = pd.DataFrame(columns=['Folders', 'Value'])

  for root, dir, files in os.walk(imagepath):
      paths = [os.path.join(root, f) for f in files]
      folders = [p.rsplit('/',1)[0] for p in paths]
      imagedays = [dt.datetime.strptime(f.rsplit('/', 1)[-1], '%Y-%m-%d') for f in folders]
      value = [dt.datetime.timestamp(dt.datetime.strptime(f.rsplit('/', 1)[-1], '%Y-%m-%d')) for f in folders]
      df1 = pd.DataFrame({'Folders': imagedays,
                          'Value': value}, 
                          index=imagedays)
      df2 = df2.append(df1)
  
  # print(df2)
  # df2.date_time = pd.to_datetime(df2.date_time)
  # df2 = df2.set_index('Folders')
  df2 = df2.sort_values(by=['Folders'])
  # print(df2)
  df2 = df2[~df2.index.duplicated(keep='first')]
  # print(df2)
  daterange = pd.date_range(start=min(df2.index), end=max(df2.index))
  # print(daterange)
  df2 = df2.reindex(daterange)
  # print(df2)

  df_head = pd.DataFrame({'Value': [1, np.nan]}, index=[dt.datetime(1970,1,1), df2.index[0]])
  df_tail = pd.DataFrame({'Value': [np.nan, np.nan, 1]}, index=[df2.index[-1], endtime, endtime])
  df2 = pd.concat([df_head, df2, df_tail], sort=True)
  # print(df2)

  nodata = np.isnan(df2['Value'])
  # print(nodata)
  # print(nodata.index)

  flags = np.convolve(np.ravel(1 * nodata),[-1,1])[1:]
  t1 = nodata.index.astype(object)[flags==-1]
  t2 = nodata.index.astype(object)[flags==1]
  return t1, t2


def dataavailability_flags(df_col,daily=False):
  #df_col = df['position []']
  import numpy as np
  import pandas as pd
  import datetime as dt

  if daily:
    df_col = df_col.apply (pd.to_numeric, errors='coerce')
    df_col = df_col.dropna()
    df_col = df_col.groupby(pd.Grouper(freq='1440Min')).aggregate(np.mean)
    # print(df_col)

  df_head = pd.DataFrame([1,np.nan], index=[dt.datetime(1970,1,1,1),df_col.index[0]])
  df_tail = pd.DataFrame([np.nan, np.nan, 1], index=[df_col.index[-1], dt.datetime(2050,1,1,1), dt.datetime(2050,1,1,1)])
  df_col = pd.concat([df_head[0], df_col, df_tail[0]])
  # print(df_col)

  nodata = np.isnan(df_col)#[1:]
  # print(nodata)

  flags = np.convolve(1 * nodata,[-1,1])[1:]
  t1 = nodata.index[flags==-1]
  t2 = nodata.index[flags==1]
  return t1, t2


def nodata_rectangles(ax, t1, t2, facecolor='gray', alpha=0.5):
  import numpy as np
  import matplotlib.transforms as mtransforms

  trans = mtransforms.blended_transform_factory(ax.transData, ax.transAxes)
  for i in range(np.min([len(t1),len(t2)])):
    ax.fill_between([t1[i],t2[i]],0,1,facecolor=facecolor, alpha=alpha, transform=trans)



def colorbar_hydrologicalyears(color_list,year_list):
    import numpy as np
    import matplotlib as mpl
    import matplotlib.pyplot as plt

    from mpl_toolkits.axes_grid1 import make_axes_locatable
    divider = make_axes_locatable(plt.gca())
    ax_cb = divider.new_horizontal(size="5%", pad=0.05)    
    cmap = mpl.colors.ListedColormap(color_list)
    bounds = np.array(year_list + [year_list[-1] + 1]) - 0.25
    cb1 = mpl.colorbar.ColorbarBase(ax_cb, cmap=cmap, boundaries=bounds, ticks=year_list, orientation='vertical')
    plt.gcf().add_axes(ax_cb)
