'''
Copyright (c) 2022, Swiss Federal Institute of Technology (ETH Zurich)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@date: February 24, 2022
@author: Samuel Weber, Jan Beutel and Matthias Meyer
'''
import concurrent.futures
import urllib3
import http

def format2iso(df, deployment, vsensor):
    import pandas as pd
    import numpy as np
    import datetime as dt
    
    df_iso = df.copy()
    colnames = pd.read_csv('./metadata/vsensor_metadata/{:s}_{:s}.csv'.format(deployment, vsensor), skiprows=0)
    columns_name = colnames['colname_new'].values
    columns_unit = colnames['unit'].values
    
    columns_old = np.array(df_iso.columns)
    columns_new = columns_old 
    for col, colname in enumerate(columns_old):
        i = np.where(columns_name == colname)[0][0]
        columns_new[col] = '{:s} {:s}'.format(columns_name[i],columns_unit[i])
        df_iso.columns = columns_new
    
    df_iso.index = df_iso.index.map(lambda x: dt.datetime.strftime(x, '%Y-%m-%dT%H:%M:%SZ'))
    df_iso.index.name = 'time'
    return df_iso


def save_data(df, path_data_file, depo, vsensor, deployment, yearly=False, iso=False, file_type='hdf', float_format=5):
    import os as os
    import numpy as np
    
    os.makedirs(path_data_file, exist_ok=True)
    
    file_name = '{:s}_{:s}'.format(depo, vsensor)
    
    if yearly == False:
        if iso:
            df = format2iso(df, deployment, vsensor)
        if file_type == 'hdf':
            df.to_hdf('{:s}{:s}.h5'.format(path_data_file, file_name), depo, mode='w')
        elif file_type == 'csv':
            df.to_csv('{:s}{:s}.csv'.format(path_data_file, file_name), mode='w', float_format='%.{:d}f'.format(float_format))
        else:
            print('Variable \'file_type={:s}\' is not defined in function \'save_data\'.'.format(file_type))
    elif yearly == True:
        for year in np.unique(df.index.year):
            df_year = df[df.index.year == year]
            if iso:
                df_year = format2iso(df_year, deployment, vsensor)
                
            if file_type == 'hdf':
                df_year.to_hdf('{:s}{:s}_{:d}.h5'.format(path_data_file, file_name, year), depo, mode='w')
            elif file_type == 'csv':
                df_year.to_csv('{:s}{:s}_{:d}.csv'.format(path_data_file, file_name, year), mode='w', float_format='%.{:d}f'.format(float_format))
            else:
                print('Variable \'file_type={:s}\' is not defined in function \'save_data\'.'.format(file_type))
    

def load_data(path_data_file, depo, vsensor, year=[], file_type='csv'):
    import pandas as pd

    if type(year) is list and len(year) == 0:
        file_name = ['{:s}_{:s}'.format(depo, vsensor)]
    elif type(year) is list and len(year) == 1:
        file_name = ['{:s}_{:s}_{:d}'.format(depo, vsensor, year[0])]
    elif type(year) is list and len(year) > 1:
        file_name = []
        for y in year:
            file_name.append('{:s}_{:s}_{:d}'.format(depo, vsensor, y))
    elif type(year) is int:
        file_name = ['{:s}_{:s}_{:d}'.format(depo, vsensor, year)]
    else:
        print('Parameters are not set correctly.')
    
    df_all = pd.DataFrame()
    for fn in file_name:
        df_year = []
        try:
            if file_type == 'hdf':
                df_year = pd.read_hdf('{:s}{:s}.h5'.format(path_data_file, fn), depo)
            elif file_type == 'csv':
                df_year = pd.read_csv('{:s}{:s}.csv'.format(path_data_file, fn), low_memory=False)
                
                # print('Hack: Remove this condition, when all data is imported again.')
                if 'Unnamed: 0' in df_year.columns:
                    df_year['time'] = pd.to_datetime(df_year['Unnamed: 0'])
                    df_year.drop(['Unnamed: 0'], axis=1, inplace=True)
                elif 'time' in df_year.columns:
                    df_year['time'] = pd.to_datetime(df_year['time'])
                    
                df_year = df_year.set_index('time')
            else:
                print('Variable \'file_type={:s}\' is not defined in function \'load_data\'.'.format(file_type))
        except:
            pass
        if len(df_year) > 1:
            df_all = pd.concat([df_all, df_year], sort=False)
    return df_all


def get_GSNdata(label, depo, deployment, position, vsensor, tbeg, tend, download_max_size):
    """ 
    Get GSNdata with HTTP access
    :param deployment: PermaSense deployment (matterhorn, jungfraujoch, dirruhorn, ?)
    :type deployment: String
    :param position: Position (location) number
    :type position: integer
    :param vsensor: Virtual sensor name (check GSN)
    :type vsensor: String
    :param tbeg tend: Start and end time in UTC (datetime.datetime(year,month,day,hour,minute,second), e.g. datetime.datetime(2015,8,1,0,0,0) '2015-08-01T00:00:00')
    :returns: Timeseries (pandas dataframe)
    
    Problems with plotting 
    http://apache-zeppelin-users-incubating-mailing-list.75479.x6.nabble.com/Issue-with-matplotlib-in-Zeppelin-when-rendering-time-series-plots-td5139.html
    https://issues.apache.org/jira/browse/ZEPPELIN-1904
    """
    import time
    import pandas as pd
    import numpy as np

    # 1 - DEFINE VSENSOR-DEPENDENT COLUMNS
    colnames = pd.read_csv('./metadata/vsensor_metadata/{:s}_{:s}.csv'.format(deployment, vsensor), skiprows=0)
    columns_old = colnames['colname_old'].values
    columns_new = colnames['colname_new'].values
    if len(columns_old) != len(columns_new):
        print('WARNING: Length of \'columns_old\' ({:d}) is not equal length of  \'columns_new\' ({:d})'.format(len(columns_old),len(columns_new)))
        
    # 2 - DEFINE CONDITIONS AND CREATE HTTP QUERY
    server = "http://data.permasense.ch/"
    # server = "https://pbl.permasense.uibk.ac.at/"
    
    # Create virtual_sensor    
    virtual_sensor = deployment + '_' + vsensor
    
    # Create query and add time as well as position selection
    query= 'vs[1]={:s}&time_format=iso&timeline=generation_time&field[1]=All&from={:s}&to={:s}&c_vs[1]={:s}&c_join[1]=and&c_field[1]=position&c_min[1]={:02d}&c_max[1]={:02d}'.format(virtual_sensor, tbeg.strftime('%d/%m/%Y+%H:%M:%S'), tend.strftime('%d/%m/%Y+%H:%M:%S'), virtual_sensor, int(position)-1, int(position))
    
    # Query extension for images, min/max size specified for selecting only NEF images or all images
    if vsensor == 'binary__mapped':
        query = query + '&vs[2]={:s}&field[2]=relative_file&c_join[2]=and&c_vs[2]={:s}&c_field[2]=file_complete&c_min[2]=0&c_max[2]=1&vs[3]={:s}&field[3]=generation_time&c_join[3]=and&c_vs[3]={:s}&c_field[3]=file_size&c_min[3]={:d}&c_max[3]=%2Binf&download_format=csv'.format(virtual_sensor, virtual_sensor, virtual_sensor, virtual_sensor, download_max_size)
    
    # Construct url:
    start_time = time.time()
    url = server  +  "multidata?"  +  query 

    # 3 - ACCESS DATA AND CREATE PANDAS DATAFRAME
    try:
        d = []

        try:
            d = pd.read_csv(url,skiprows=2, low_memory=False)
        except pd.errors.EmptyDataError:
            df = pd.DataFrame()
            return df # will skip the rest of the block and move to next file

        query_time = time.time() - start_time

        start_time = time.time()
        df = pd.DataFrame(columns=columns_new)
        df.time = pd.to_datetime(d.generation_time)

        for i in range(len(columns_old)):
            if columns_new[i] != 'time':
                setattr(df, columns_new[i], getattr(d,columns_old[i]))
        conversion_time = time.time() - start_time
        
        start_time = time.time()
        df = df.sort_values(by='time')

    except:
        df = pd.DataFrame(np.zeros([1,len(columns_new)]), columns=columns_new)
    
    # Remove columns with names 'delete'
    try:
        df.drop(['delete'], axis=1, inplace=True)
    except:
        pass
    
    # Remove columns with only 'null'
    df = df.replace(r'null', np.nan, regex=True)
    isnull = df.isnull().all()
    [df.drop([col_name], axis=1, inplace=True) for col_name in df.columns[isnull]]

    df.time = pd.to_datetime(df.time, utc=True)
    df = df.set_index('time')
    
    sorting_time = time.time() - start_time
    df_length = len(df)
    print('Data from {:s}, position {:s}, {:s}'.format(label, depo, vsensor), '- Query returned %d'% df_length, 'lines. Query time: {:.2f}'.format(query_time), 'Conversion time: {:.2f}'.format(conversion_time), 'Sorting time: {:.2f}'.format(sorting_time))

    return df
    

#%%
def filter_data(df_raw, depo, path_metadata='./'):
    '''
    Filter raw data according sensortype-specific reference resistivity measurements
    '''
    import numpy as np
    import pandas as pd
    import datetime as dt
    from pytz import timezone

    # Load all sensor metadata
    sensor_metadata = pd.read_csv(path_metadata + 'metadata/filter_metadata.csv')
    
    # Select position (sensortype) specific reference resistivity values 
    sensor_metadata_sel = sensor_metadata.loc[(sensor_metadata['depo']==depo.lower())] # 
        
    if np.isnan(np.sum(sensor_metadata_sel['ref1'])) or ('ref1' not in df_raw.dtypes):
        print('No reference/calibration values are given...')
        return df_raw
    
    else:
        # Create empty dataframe
        df_filt_all = pd.DataFrame()
        
        # Loop through all sensors 
        for index, row in sensor_metadata_sel.iterrows():
            # Define time range
            t1 = row['time_beg']
            t2 = row['time_end']
            if isinstance(t1, str):
                tb = dt.datetime.strptime(t1, '%Y-%m-%d')
                tb = timezone('UTC').localize(tb)
            else:
                tb = dt.datetime(2000, 1, 1, tzinfo=timezone('UTC'))
            if isinstance(t2, str):
                te = dt.datetime.strptime(t2, '%Y-%m-%d')
                te = timezone('UTC').localize(te)
            else:
                te = dt.datetime(2100, 1, 1, tzinfo=timezone('UTC'))
                
            # Crop dataframe to valid time range
            print('Crop time interval: ',tb, te)
            df_red = df_raw[(df_raw.index > tb) & (df_raw.index < te)]
            
            # Define lower ('r*low') and upper ('r*high') limit of tolerance
            r1low  = row['ref1'] - row['dref1']
            r1high = row['ref1'] + row['dref1']
            r2low  = row['ref2'] - row['dref2']
            r2high = row['ref2'] + row['dref2']
            r6low  = row['ref6'] - np.float(row['dref6'])
            r6high = row['ref6'] + np.float(row['dref6'])
            
            # Filter according to these limits
            if row['sensortype'] in ['sensorrod','moisturechain']:
                df_filt = df_red.loc[(df_red['ref1'] > r1low) & (df_red['ref1'] < r1high ) & (df_red['ref6'] > r6low ) & (df_red['ref6'] < r6high )]
            elif row['sensortype'] in ['sensorrod_ETH_6t']:
                df_filt = df_red.loc[(df_red['ref1'] > r1low ) & (df_red['ref1'] < r1high ) & (df_red['ref2'] > r2low ) & (df_red['ref2'] < r2high )]
            elif row['sensortype'] == 'thermistorchain': # only one reference value is available
                df_filt = df_red.loc[(df_red['ref1'] > r1low ) & (df_red['ref1'] < r1high )]
            elif row['sensortype'] == 'sensorrod_UMS_th3': # no reference value is available
                df_filt = df_red.copy()
                
            if len(df_filt) > 1:
                df_filt_all = pd.concat([df_filt_all, df_filt], sort=False)
            print('After filter')

        for colname in ['temperature_nearsurface_t2', 'temperature_nearsurface_t1']:
            if colname in df_raw.columns and ~df_raw[colname].isnull().all():
                df_sel = pd.DataFrame(df_raw[colname][~df_raw[colname].isnull()], columns=[colname])
                print('inside')
                print(df_sel)
                df_filt_all = df_sel.append(df_filt_all, sort=False)
                first_column = df_filt_all.pop('position')
                df_filt_all.insert(0, 'position', first_column)
                
                # Change order of columns
                cols = df_filt_all.columns.tolist()
                cols = cols[-1:] + cols[:-1]
                df_filt_all = df_filt_all[cols]  #    OR    df_filt_all = df_filt_all.ix[:, cols]
        return df_filt_all


def clean_data(df_raw, depo, vsensor, path_metadata='./'):
    '''
    Clean data according manual interaction.
    '''
    import numbers
    import numpy as np
    import pandas as pd
    import datetime as dt
    
    df_cleaned = df_raw.copy()
    
    clean_list = pd.read_csv(path_metadata + 'metadata/clean_metadata.csv')
    clean_depo = clean_list.loc[(clean_list.depo==depo.lower()) & (clean_list.vsensor==vsensor)]
    
    for i in clean_depo.index:
        clean_sel = clean_depo[clean_depo.index==i]
        
        # Define time range
        if isinstance(clean_sel['time_beg'].values[0], str):
            tb = dt.datetime.strptime(clean_sel['time_beg'].values[0], '%Y-%m-%d')
            tb = pd.to_datetime(tb, utc=True)
        else:
            tb = pd.to_datetime(dt.datetime(2000,1,1), utc=True)
        if isinstance(clean_sel['time_end'].values[0], str):
            te = dt.datetime.strptime(clean_sel['time_end'].values[0], '%Y-%m-%d')
            te = pd.to_datetime(te, utc=True)
        else:
            te = pd.to_datetime(dt.datetime(2100,1,1), utc=True)

        # Index of time condition
        ind_time = (df_cleaned.index > tb) & (df_cleaned.index < te)

        # Index of condition
        if clean_sel['var'].values[0] == '*':
            ind_cond = np.ones(len(ind_time), dtype=bool)
        elif isinstance(clean_sel['cond_op'].values[0], str) and isinstance(clean_sel['cond_par'].values[0], numbers.Number):
            if clean_sel['cond_op'].values[0] == '>':
                ind_cond = (df_cleaned[clean_sel['var'].values[0]] > np.float(clean_sel['cond_par'].values[0]))
            elif clean_sel['cond_op'].values[0] == '=':
                ind_cond = (df_cleaned[clean_sel['var'].values[0]] == np.float(clean_sel['cond_par'].values[0]))
            elif clean_sel['cond_op'].values[0] == '<':
                ind_cond = (df_cleaned[clean_sel['var'].values[0]] < np.float(clean_sel['cond_par'].values[0]))
            ind_cond = ind_cond.values
        else:
            ind_cond = np.ones(len(ind_time), dtype=bool)

        # Apply operation
        ind = ind_time * ind_cond
        if clean_sel['var'].values[0] == '*':
            if clean_sel['op'].values[0] == 'ok':
                print('No cleaning...')
            elif clean_sel['op'].values[0] == 'del':
                df_cleaned.drop(df_cleaned.index[ind], inplace=True)
            else:
                print('The combination of var=\'*\' and op=\'{:s}\' is not valid (depo=\'{:s}\', vsensor=\'{:s}\').'.format(clean_sel['op'].values[0], depo, vsensor))
        else:
            if clean_sel['op'].values[0] == 'ok':
                print('No cleaning...')
            elif clean_sel['op'].values[0] == 'del':
                print('deleting some values')
                df_cleaned.loc[ind, clean_sel['var'].values[0]] = df_cleaned.loc[ind, clean_sel['var'].values[0]] + np.NaN
            elif clean_sel['op'].values[0] == 'offset':
                df_cleaned.loc[ind, clean_sel['var'].values[0]] = df_cleaned.loc[ind, clean_sel['var'].values[0]] + clean_sel['par'].values[0]
            elif clean_sel['op'].values[0] == 'mult':
                df_cleaned.loc[ind, clean_sel['var'].values[0]] = df_cleaned.loc[ind, clean_sel['var'].values[0]] * clean_sel['par'].values[0]
            elif clean_sel['op'].values[0] == 'replace':
                print(df_cleaned.loc[ind, clean_sel['var'].values[0]])
                print(clean_sel['par'].values[0])
                df_cleaned.loc[ind, clean_sel['var'].values[0]] = df_cleaned.loc[ind, clean_sel['var'].values[0]] * 0 + clean_sel['par'].values[0]
        
    return df_cleaned


def fetch_GSNimg(job):
    import os as os
    import numpy as np
    import pandas as pd
    import subprocess, shlex
    import piexif
    import time
    import imageio

    start_time = time.time()
    orig_filename, query, pknum, path, fname, extension, resize, resize_w, resize_h = job

    if len(fname)==15:
        fname = pd.to_datetime(fname, format='%Y%m%d_%H%M%S')
    elif len(fname)==17:
        fname = pd.to_datetime(fname, format='%Y-%m-%d_%H%M%S')
    elif len(fname)==21:
        #print(row.fname[:-4])
        fname = pd.to_datetime(fname[:-4], format='%Y-%m-%d_%H%M%S')
    elif len(fname)==30:
        print(fname[:-15])
        fname = pd.to_datetime(fname[:-15], format='%Y%m%d_%H%M%S')
    else:
        print('Binary file name is not recognized %s %s'% (fname, pknum))
        print(fname)
        return

    dst = path + fname.strftime("%Y-%m-%d")
    os.makedirs(dst, exist_ok=True)

    src = dst + '/' + fname.strftime("%Y%m%d_%H%M%S")

    url = 'http://data.permasense.ch/field?vs=matterhorn_binary__mapped&field=data&pk={:s}'.format(pknum)
    filename = src + '.' + extension

    global http
    if not http:
        http = urllib3.PoolManager(num_pools=1000, maxsize=100000)
    try:
        resp = http.request('GET', url)
    except Exception as e:
        print(e)
        # Retry once, good enough for now.
        resp = http.request('GET', url)
        print('Retry done.')

    try:
        if extension in ['.JPG','.jpg','JPG','jpg']:
            with open(filename, "wb") as f:
                print('JPEG File')
                f.write(resp.data)
    
            outfile = src + '_new.JPG'
            # print(outfile)
            if resize:
                cmd = shlex.split('./NConvert/nconvert -quiet -resize %d %d -out jpeg -overwrite -o %s %s' % (resize_h, resize_w, outfile, filename))
            else:
                cmd = shlex.split('./NConvert/nconvert -quiet -out jpeg -overwrite -o %s %s' % (outfile, filename))
            subprocess.run(cmd, stdout=subprocess.PIPE)
    
        elif extension in ['.NEF','.nef','NEF','nef','.TIF','TIF']:
            with open(filename, "wb") as f:
                f.write(resp.data)
    
            outfile = src + '.JPG'
            # print(outfile)
            if resize:
                cmd = shlex.split('./NConvert/nconvert -quiet -out jpeg -resize %d %d -overwrite -o %s %s' % (resize_h, resize_w, outfile, filename))
            else:
                cmd = shlex.split('./NConvert/nconvert -quiet -embedded_jpeg -rotate 90 -dpi 300 -out jpeg -overwrite -o %s %s' % (outfile, filename))
                # cmd = shlex.split('./NConvert/nconvert -quiet -out jpeg -overwrite -o %s %s' % (dst,dst))
            subprocess.run(cmd, stdout=subprocess.PIPE)
    
        else:
            print('The file has an unknown extension an will be ignored: %s %s %s'% (src, extension, pknum))
            return filename, pknum, resp.data
    except:
        print('Did you download the NConvert (batch image processor) and copied it into the folder \"gsn_data_management_bin\"?')
        pass

    exif_dict = piexif.load(outfile)
    exif_dict["0th"][piexif.ImageIFD.Orientation] = 8
    piexif.insert(piexif.dump(exif_dict),outfile)

    # Detect bad images
    f = imageio.imread(outfile, as_gray=True)
    mean = np.mean(f)
    std = np.std(f)
    # print(std)
    if std > 0:
        snr = mean/std
    else:
        #find all white images
        snr = 1000
        print('Removing, the image signal-to-noise is: %s %s %s %s %s'% (outfile, pknum, mean, std, snr))
        os.remove(outfile)
    return filename, pknum, resp.data


def get_GSNimg(path_img, deployment, label, depo, position, vsensor, tbeg, tend, keep_nef, download_max_size, resize, resize_w, resize_h):
    import os as os
    import math as math
    import multiprocessing

    df = get_GSNdata(label, depo, deployment, position, vsensor, tbeg, tend, download_max_size)
    df['pk'] = df.data.str.split('=', expand=True)[3]
    df['path'] = path_img
    df['fname'] = df.relative_file.str.split('/', expand=True)[2].str.split('.', expand=True)[0]
    df['ext'] = df.relative_file.str.split('/', expand=True)[2].str.split('.', expand=True)[1]
    df['time'] = df.relative_file.str.split('/', expand=True)[2].str.split('.', expand=True)[0]
    df['resize'] = resize
    df['resize_w'] = resize_w
    df['resize_h'] = resize_h
    df = df.set_index('time')
    print()
    print('Retrieving %s Images from server.'% len(df.values))

    global http
    http = None

	# let's not overwhelm the python scheduler
    CHUNKSIZE = 2000
    CHUNKSIZE = 4
    MAX_NUM_WORKERS = 32 #16
    WORKERS = min([math.floor(multiprocessing.cpu_count()*1/2), MAX_NUM_WORKERS])

    print('Now doing concurrent downloads on %s worker threads, chunksize %s'% (WORKERS, CHUNKSIZE))
    executor = concurrent.futures.ProcessPoolExecutor(max_workers=WORKERS)
   
    for filename, pknum, data in executor.map(fetch_GSNimg, df.values, chunksize=CHUNKSIZE):
        try:
            if not keep_nef:
                os.remove(filename)
            else:
                print(filename, os.path.getsize(filename))

        except:
            print('File delete did not work for %s %s'% (filename, pknum))
            pass


def multi_fetch_GSN_data(job, PATH_DATA, DOWNLOAD_MAX_SIZE, tbeg, tend):
    import time

    start_time = time.time()
    depo, depo_line = job

    for label in depo_line:
        if depo[0:2] == 'JJ':
            deployment = 'jungfraujoch'
        elif depo[0:2] == 'AD':
            deployment = 'adm'
        elif depo[0:2] == 'MH':
            deployment = 'matterhorn'
        elif depo[0:2] == 'DH':
            deployment = 'dirruhorn'
        elif depo[0:2] == 'PE':
            deployment = 'permos'
        position = int(depo[2:4])
        
    # Query data from GSN server and save it locally
    # Loop through values for a given key of dictionary DEPO_VS
    for vsensor in sorted(list(depo_line[label])):
        print('Data from {:s}, position {:s}, {:s} is now being imported.'.format(label, depo, vsensor))
        df = get_GSNdata(label, depo, deployment, position, vsensor, tbeg, tend, download_max_size = DOWNLOAD_MAX_SIZE)
        len_data = len(df)
        if len_data > 1:
            start_time = time.time()
            if vsensor == 'gps_differential__batch__daily':
                save_data(df, PATH_DATA + '/gnss_data_raw/', label, vsensor, deployment, file_type='csv')
                save_data(df, PATH_DATA + '/gnss_data_raw/', label, vsensor, deployment, yearly=True, iso=True, file_type='csv')
            elif vsensor == 'gps_differential__rtklib__daily':
                save_data(df, PATH_DATA + '/gnss_data_raw/', label, vsensor, deployment, file_type='csv')
                save_data(df, PATH_DATA + '/gnss_data_raw/', label, vsensor, deployment, yearly=True, iso=True, file_type='csv')
            else:
                save_data(df, PATH_DATA + '/timeseries_data_raw/', label, vsensor, deployment, file_type='csv')
                save_data(df, PATH_DATA + '/timeseries_data_raw/', label, vsensor, deployment, yearly=True, iso=True, file_type='csv')
            storage_time = time.time() - start_time
            print('Storage time: {:.2f}'.format(storage_time))
        else:
            print()


    return deployment, position, vsensor, len_data
